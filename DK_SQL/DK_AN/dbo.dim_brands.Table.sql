USE [DK_AN]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dim_brands](
	[id] [int] NOT NULL,
	[brand_name] [nvarchar](120) NULL,
	[country_brand_origin] [nvarchar](120) NULL,
	[continent] [nvarchar](120) NULL,
	[domestic_foreign] [nvarchar](120) NULL,
	[brand_price_segment] [nvarchar](120) NULL,
 CONSTRAINT [dim_brands_pk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
