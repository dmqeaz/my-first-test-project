USE [DK_AN]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dim_cars](
	[id] [bigint] NOT NULL,
	[vin_code] [nvarchar](40) NULL,
	[engine_number] [nvarchar](40) NULL,
	[body_number] [nvarchar](40) NULL,
	[year_of_manufacture] [nvarchar](8) NULL,
	[fuel_type] [nvarchar](40) NULL,
	[engine_displacement] [int] NULL,
	[engine_displacement_range] [nvarchar](40) NULL,
	[steering_wheel] [nvarchar](40) NULL,
	[gross_vehicle_weight] [nvarchar](40) NULL,
	[curb_weight] [nvarchar](40) NULL,
	[country_of_manufacture_vin_code] [nvarchar](80) NULL,
	[manufacturer] [nvarchar](400) NULL,
	[country_of_brand_origin_chassis] [nvarchar](80) NULL,
	[chassi_number] [nvarchar](40) NULL,
	[engine_type] [nvarchar](40) NULL,
	[gvw_range_2] [nvarchar](40) NULL,
	[gvw_range_3] [nvarchar](40) NULL,
	[gvw_range_4] [nvarchar](40) NULL,
	[gvw_rating] [nvarchar](40) NULL,
	[gvw_class] [nvarchar](40) NULL,
	[engine_power] [int] NULL,
 CONSTRAINT [dim_cars_PK] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
