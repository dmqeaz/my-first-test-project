USE [DK_AN]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dim_company_groups](
	[id] [int] NOT NULL,
	[group_rus] [nvarchar](200) NULL,
	[group_eng] [nvarchar](200) NULL,
	[main_company_tin] [nvarchar](200) NULL,
 CONSTRAINT [dim_company_groups_pk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
