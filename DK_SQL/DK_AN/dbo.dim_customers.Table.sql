USE [DK_AN]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dim_customers](
	[id] [int] NOT NULL,
	[unique_tin] [nvarchar](20) NULL,
	[correct_tin] [nvarchar](20) NULL,
	[customer_name] [nvarchar](510) NULL,
	[customer_short_name] [nvarchar](510) NULL,
	[okved_code_new] [nvarchar](40) NULL,
	[okved_name_new] [nvarchar](1000) NULL,
	[okved_code_old] [nvarchar](40) NULL,
	[okved_name_old] [nvarchar](1000) NULL,
	[company_size] [nvarchar](40) NULL,
	[revenue] [float] NULL,
	[number_of_employee] [nvarchar](200) NULL,
	[net_profit] [nvarchar](200) NULL,
	[gross_profitability] [float] NULL,
	[ros] [int] NULL,
	[index_of_financial_risks] [int] NULL,
	[registration_number] [nvarchar](30) NULL,
	[contact_person] [nvarchar](200) NULL,
	[adress] [nvarchar](510) NULL,
	[phone] [nvarchar](1022) NULL,
	[site] [nvarchar](510) NULL,
	[e_mail] [nvarchar](2046) NULL,
 CONSTRAINT [dim_customres_pk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
