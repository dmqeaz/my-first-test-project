USE [DK_AN]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dim_locations](
	[id] [int] NOT NULL,
	[region] [nvarchar](120) NULL,
	[correct_region] [nvarchar](120) NULL,
	[economic_region] [nvarchar](120) NULL,
	[federal_district] [nvarchar](120) NULL,
	[correct_federal_district] [nvarchar](120) NULL,
 CONSTRAINT [dim_locations_pk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
