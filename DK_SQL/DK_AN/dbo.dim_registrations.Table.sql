USE [DK_AN]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dim_registrations](
	[id] [bigint] NOT NULL,
	[new_or_used] [nvarchar](40) NULL,
	[registration_type] [nvarchar](60) NULL,
	[type_of_owner] [nvarchar](40) NULL,
	[district_for_moscow] [nvarchar](120) NULL,
	[district_in_the_region] [nvarchar](120) NULL,
	[district_in_the_city_or_town] [nvarchar](120) NULL,
	[city_or_town] [nvarchar](120) NULL,
	[settlement] [nvarchar](120) NULL,
	[okato] [nvarchar](40) NULL,
	[oktmo] [nvarchar](40) NULL,
	[regional_center_or_region] [nvarchar](120) NULL,
	[street] [nvarchar](120) NULL,
	[code_for_streets] [nvarchar](400) NULL,
	[post_code] [nvarchar](40) NULL,
	[lcv] [nvarchar](10) NULL,
 CONSTRAINT [dim_registrations_PK] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
