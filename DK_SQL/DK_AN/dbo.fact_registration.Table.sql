USE [DK_AN]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fact_registration](
	[date_id] [date] NOT NULL,
	[brand_id] [int] NOT NULL,
	[locanion_id] [int] NOT NULL,
	[model_id] [int] NOT NULL,
	[registration_id] [bigint] NOT NULL,
	[dealer_id] [int] NOT NULL,
	[product_segment_id] [int] NOT NULL,
	[price_segment_id] [int] NOT NULL,
	[retail_kam_id] [int] NOT NULL,
	[version_id] [int] NOT NULL,
	[car_id] [bigint] NOT NULL,
	[company_id] [int] NOT NULL,
	[quantity] [int] NULL,
	[mbfsr_id] [int] NOT NULL,
	[company_group_id] [int] NOT NULL,
	[error_code] [int] NULL,
	[axel_type_id] [int] NOT NULL,
	[body_type_id] [int] NOT NULL,
	[weight_segment_id] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[fact_registration]  WITH CHECK ADD  CONSTRAINT [fact_registration_dim_axle_types_FK] FOREIGN KEY([axel_type_id])
REFERENCES [dbo].[dim_axle_types] ([id])
GO
ALTER TABLE [dbo].[fact_registration] CHECK CONSTRAINT [fact_registration_dim_axle_types_FK]
GO
ALTER TABLE [dbo].[fact_registration]  WITH CHECK ADD  CONSTRAINT [fact_registration_dim_body_types_FK] FOREIGN KEY([body_type_id])
REFERENCES [dbo].[dim_body_types] ([id])
GO
ALTER TABLE [dbo].[fact_registration] CHECK CONSTRAINT [fact_registration_dim_body_types_FK]
GO
ALTER TABLE [dbo].[fact_registration]  WITH CHECK ADD  CONSTRAINT [fact_registration_dim_brands_fk] FOREIGN KEY([brand_id])
REFERENCES [dbo].[dim_brands] ([id])
GO
ALTER TABLE [dbo].[fact_registration] CHECK CONSTRAINT [fact_registration_dim_brands_fk]
GO
ALTER TABLE [dbo].[fact_registration]  WITH CHECK ADD  CONSTRAINT [fact_registration_dim_cars_FK] FOREIGN KEY([car_id])
REFERENCES [dbo].[dim_cars] ([id])
GO
ALTER TABLE [dbo].[fact_registration] CHECK CONSTRAINT [fact_registration_dim_cars_FK]
GO
ALTER TABLE [dbo].[fact_registration]  WITH CHECK ADD  CONSTRAINT [fact_registration_dim_company_groups_FK] FOREIGN KEY([company_group_id])
REFERENCES [dbo].[dim_company_groups] ([id])
GO
ALTER TABLE [dbo].[fact_registration] CHECK CONSTRAINT [fact_registration_dim_company_groups_FK]
GO
ALTER TABLE [dbo].[fact_registration]  WITH CHECK ADD  CONSTRAINT [fact_registration_dim_customers_FK] FOREIGN KEY([company_id])
REFERENCES [dbo].[dim_customers] ([id])
GO
ALTER TABLE [dbo].[fact_registration] CHECK CONSTRAINT [fact_registration_dim_customers_FK]
GO
ALTER TABLE [dbo].[fact_registration]  WITH CHECK ADD  CONSTRAINT [fact_registration_dim_dealers_FK] FOREIGN KEY([dealer_id])
REFERENCES [dbo].[dim_dealers] ([id])
GO
ALTER TABLE [dbo].[fact_registration] CHECK CONSTRAINT [fact_registration_dim_dealers_FK]
GO
ALTER TABLE [dbo].[fact_registration]  WITH CHECK ADD  CONSTRAINT [fact_registration_dim_locations_fk] FOREIGN KEY([locanion_id])
REFERENCES [dbo].[dim_locations] ([id])
GO
ALTER TABLE [dbo].[fact_registration] CHECK CONSTRAINT [fact_registration_dim_locations_fk]
GO
ALTER TABLE [dbo].[fact_registration]  WITH CHECK ADD  CONSTRAINT [fact_registration_dim_mbfsr_FK] FOREIGN KEY([mbfsr_id])
REFERENCES [dbo].[dim_mbfsr] ([id])
GO
ALTER TABLE [dbo].[fact_registration] CHECK CONSTRAINT [fact_registration_dim_mbfsr_FK]
GO
ALTER TABLE [dbo].[fact_registration]  WITH CHECK ADD  CONSTRAINT [fact_registration_dim_models_fk] FOREIGN KEY([model_id])
REFERENCES [dbo].[dim_models] ([id])
GO
ALTER TABLE [dbo].[fact_registration] CHECK CONSTRAINT [fact_registration_dim_models_fk]
GO
ALTER TABLE [dbo].[fact_registration]  WITH CHECK ADD  CONSTRAINT [fact_registration_dim_price_segments_FK] FOREIGN KEY([price_segment_id])
REFERENCES [dbo].[dim_price_segments] ([id])
GO
ALTER TABLE [dbo].[fact_registration] CHECK CONSTRAINT [fact_registration_dim_price_segments_FK]
GO
ALTER TABLE [dbo].[fact_registration]  WITH CHECK ADD  CONSTRAINT [fact_registration_dim_product_segments_FK] FOREIGN KEY([product_segment_id])
REFERENCES [dbo].[dim_product_segments] ([id])
GO
ALTER TABLE [dbo].[fact_registration] CHECK CONSTRAINT [fact_registration_dim_product_segments_FK]
GO
ALTER TABLE [dbo].[fact_registration]  WITH CHECK ADD  CONSTRAINT [fact_registration_dim_registrations_FK_1] FOREIGN KEY([registration_id])
REFERENCES [dbo].[dim_registrations] ([id])
GO
ALTER TABLE [dbo].[fact_registration] CHECK CONSTRAINT [fact_registration_dim_registrations_FK_1]
GO
ALTER TABLE [dbo].[fact_registration]  WITH CHECK ADD  CONSTRAINT [fact_registration_dim_retail_kam_FK] FOREIGN KEY([retail_kam_id])
REFERENCES [dbo].[dim_retail_kam] ([id])
GO
ALTER TABLE [dbo].[fact_registration] CHECK CONSTRAINT [fact_registration_dim_retail_kam_FK]
GO
ALTER TABLE [dbo].[fact_registration]  WITH CHECK ADD  CONSTRAINT [fact_registration_dim_time_fk] FOREIGN KEY([date_id])
REFERENCES [dbo].[dim_time] ([date_id])
GO
ALTER TABLE [dbo].[fact_registration] CHECK CONSTRAINT [fact_registration_dim_time_fk]
GO
ALTER TABLE [dbo].[fact_registration]  WITH CHECK ADD  CONSTRAINT [fact_registration_dim_versions_fk] FOREIGN KEY([version_id])
REFERENCES [dbo].[dim_versions] ([id])
GO
ALTER TABLE [dbo].[fact_registration] CHECK CONSTRAINT [fact_registration_dim_versions_fk]
GO
ALTER TABLE [dbo].[fact_registration]  WITH CHECK ADD  CONSTRAINT [fact_registration_dim_weight_segments_FK] FOREIGN KEY([weight_segment_id])
REFERENCES [dbo].[dim_weight_segments] ([id])
GO
ALTER TABLE [dbo].[fact_registration] CHECK CONSTRAINT [fact_registration_dim_weight_segments_FK]
GO
