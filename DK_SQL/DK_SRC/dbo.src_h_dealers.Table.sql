USE [DK_SRC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[src_h_dealers](
	[record_id] [bigint] IDENTITY(1,1) NOT NULL,
	[vin] [nvarchar](100) NULL,
	[engine_number] [nvarchar](100) NULL,
	[dealer_name] [nvarchar](100) NULL,
	[new_dealer_name] [nvarchar](100) NULL,
	[date_of_loading] [datetime] NULL,
	[filename] [nvarchar](100) NULL
) ON [PRIMARY]
GO
