USE [DK_SRC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[src_h_engine_and_models](
	[record_id] [bigint] IDENTITY(1,1) NOT NULL,
	[vin] [nvarchar](100) NULL,
	[engine_number] [nvarchar](100) NULL,
	[model_and_other] [nvarchar](100) NULL,
	[date_of_loading] [datetime] NULL,
	[filename] [nvarchar](16) NULL
) ON [PRIMARY]
GO
