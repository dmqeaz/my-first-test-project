USE [DK_SRC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[src_h_engine_power](
	[record_id] [bigint] IDENTITY(1,1) NOT NULL,
	[engine_power_old] [nvarchar](16) NULL,
	[correct_engine_power] [int] NULL
) ON [PRIMARY]
GO
