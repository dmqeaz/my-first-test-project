USE [DK_SRC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[src_h_lising_inn](
	[record_id] [bigint] IDENTITY(1,1) NOT NULL,
	[lising_tin] [nvarchar](12) NULL,
	[date_of_loading] [datetime] NULL,
	[filename] [nvarchar](16) NULL
) ON [PRIMARY]
GO
