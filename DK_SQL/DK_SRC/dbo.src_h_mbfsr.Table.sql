USE [DK_SRC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[src_h_mbfsr](
	[record_id] [bigint] IDENTITY(1,1) NOT NULL,
	[vin] [nvarchar](60) NULL,
	[mbfsr] [nvarchar](60) NULL,
	[date_of_loading] [datetime] NULL,
	[filename] [nvarchar](16) NULL
) ON [PRIMARY]
GO
