USE [DK_SRC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[src_h_park_for_reatai_kam](
	[record_id] [bigint] IDENTITY(1,1) NOT NULL,
	[inn] [nvarchar](30) NULL,
	[quantity] [nvarchar](30) NULL,
	[count_date] [nvarchar](20) NULL,
	[date_of_loading] [datetime] NULL,
	[filename] [nvarchar](16) NULL
) ON [PRIMARY]
GO
