USE [DK_SRC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[src_h_retail_kam_lising](
	[record_id] [bigint] IDENTITY(1,1) NOT NULL,
	[vin_code] [nvarchar](20) NULL,
	[chassi_number] [nvarchar](20) NULL,
	[kam_retail] [nvarchar](20) NULL,
	[date_of_loading] [datetime] NULL,
	[filename] [nvarchar](16) NULL
) ON [PRIMARY]
GO
