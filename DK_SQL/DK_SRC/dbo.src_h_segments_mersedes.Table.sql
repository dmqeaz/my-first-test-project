USE [DK_SRC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[src_h_segments_mersedes](
	[record_id] [bigint] IDENTITY(1,1) NOT NULL,
	[model_and_modification] [nvarchar](100) NULL,
	[segment_name] [nvarchar](100) NULL,
	[date_of_loading] [datetime] NULL,
	[filename] [nvarchar](16) NULL
) ON [PRIMARY]
GO
