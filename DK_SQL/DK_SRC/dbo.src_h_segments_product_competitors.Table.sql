USE [DK_SRC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[src_h_segments_product_competitors](
	[record_id] [bigint] IDENTITY(1,1) NOT NULL,
	[brand] [nvarchar](100) NULL,
	[model_and_modification] [nvarchar](100) NULL,
	[body_type] [nvarchar](100) NULL,
	[weight_in_a_segment_1] [nvarchar](100) NULL,
	[axle_configuration] [nvarchar](100) NULL,
	[engine_power] [nvarchar](100) NULL,
	[segments] [nvarchar](100) NULL,
	[new_segment] [nvarchar](100) NULL,
	[date_of_loading] [datetime] NULL,
	[filename] [nvarchar](100) NULL
) ON [PRIMARY]
GO
