USE [DK_SRC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[src_h_segments_product_competitors_clear](
	[record_id] [bigint] NOT NULL,
	[brand] [nvarchar](200) NULL,
	[model_and_modification] [nvarchar](200) NULL,
	[body_type] [nvarchar](200) NULL,
	[weight_in_a_segment_1] [nvarchar](200) NULL,
	[axle_configuration] [nvarchar](200) NULL,
	[engine_power] [nvarchar](200) NULL,
	[segments] [nvarchar](200) NULL,
	[new_segment] [nvarchar](200) NULL,
	[date_of_loading] [datetime] NULL,
	[filename] [nvarchar](200) NULL
) ON [PRIMARY]
GO
