USE [DK_SRC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[src_h_vin_and_spec_codes](
	[record_id] [bigint] IDENTITY(1,1) NOT NULL,
	[vin] [nvarchar](200) NULL,
	[spec_codes] [nvarchar](1500) NULL,
	[date_of_loading] [datetime] NULL,
	[filename] [nvarchar](32) NULL
) ON [PRIMARY]
GO
