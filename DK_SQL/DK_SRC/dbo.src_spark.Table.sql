USE [DK_SRC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[src_spark](
	[record_id] [bigint] IDENTITY(1,1) NOT NULL,
	[correct_taxplayer_identification_number] [nvarchar](400) NULL,
	[unique_taxplayer_identification_number] [nvarchar](400) NULL,
	[previously_taxplayer_identification_number] [nvarchar](400) NULL,
	[short_customer_number] [nvarchar](400) NULL,
	[registration_number] [nvarchar](400) NULL,
	[customer_name] [nvarchar](400) NULL,
	[adress] [nvarchar](400) NULL,
	[contact_person_head_of_company] [nvarchar](400) NULL,
	[phone] [nvarchar](400) NULL,
	[email] [nvarchar](400) NULL,
	[site] [nvarchar](400) NULL,
	[original_taxplayer_identification_number] [nvarchar](400) NULL,
	[industry_1] [nvarchar](400) NULL,
	[first_okved] [nvarchar](400) NULL,
	[old_industry_okved_first_reduction] [nvarchar](400) NULL,
	[industry_old_reduction] [nvarchar](400) NULL,
	[previous_taxplayer_identification_number] [nvarchar](400) NULL,
	[index_of_financial_risks] [nvarchar](400) NULL,
	[size_of_company] [nvarchar](400) NULL,
	[numbers_of_emploee] [nvarchar](400) NULL,
	[my_lists] [nvarchar](400) NULL,
	[revenue] [nvarchar](400) NULL,
	[gross_profitability] [nvarchar](400) NULL,
	[net_profit] [nvarchar](400) NULL,
	[ros] [nvarchar](400) NULL,
	[group_company_taxplayer_identification_number] [nvarchar](400) NULL,
	[group_company] [nvarchar](400) NULL,
	[feature_inn] [nvarchar](400) NULL,
	[date_of_adding] [nvarchar](400) NULL,
	[retail_kam] [nvarchar](400) NULL,
	[date_of_loading] [datetime] NULL,
	[filename] [nvarchar](400) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE CLUSTERED INDEX [_dta_index_src_spark_01] ON [dbo].[src_spark]
(
	[unique_taxplayer_identification_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [_dta_index_src_spark_02] ON [dbo].[src_spark]
(
	[industry_old_reduction] ASC
)
INCLUDE([old_industry_okved_first_reduction]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [_dta_index_src_spark_03] ON [dbo].[src_spark]
(
	[unique_taxplayer_identification_number] ASC
)
INCLUDE([first_okved],[industry_old_reduction],[group_company_taxplayer_identification_number],[retail_kam]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [_dta_index_src_spark_04] ON [dbo].[src_spark]
(
	[correct_taxplayer_identification_number] ASC
)
INCLUDE([unique_taxplayer_identification_number],[previously_taxplayer_identification_number],[short_customer_number],[registration_number],[customer_name],[adress],[contact_person_head_of_company],[phone],[email],[site],[original_taxplayer_identification_number],[index_of_financial_risks],[size_of_company],[numbers_of_emploee],[my_lists],[revenue],[gross_profitability],[net_profit],[ros],[feature_inn],[date_of_adding]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [_dta_index_src_spark_05] ON [dbo].[src_spark]
(
	[group_company_taxplayer_identification_number] ASC
)
INCLUDE([group_company]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [_dta_index_src_spark_06] ON [dbo].[src_spark]
(
	[first_okved] ASC
)
INCLUDE([industry_1]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
