USE [DK_SRC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[src_truck_registration](
	[record_id] [bigint] IDENTITY(1,1) NOT NULL,
	[federal_district] [nvarchar](510) NULL,
	[economic_region] [nvarchar](510) NULL,
	[region] [nvarchar](510) NULL,
	[car_type] [nvarchar](510) NULL,
	[brand] [nvarchar](510) NULL,
	[model_and_modification] [nvarchar](510) NULL,
	[model] [nvarchar](510) NULL,
	[modification] [nvarchar](510) NULL,
	[year_of_manufacture] [nvarchar](510) NULL,
	[year_of_registration] [nvarchar](510) NULL,
	[quater_of_registration] [nvarchar](510) NULL,
	[month_of_registration] [nvarchar](510) NULL,
	[day_of_registration] [nvarchar](510) NULL,
	[quantity] [nvarchar](510) NULL,
	[new_or_used] [nvarchar](510) NULL,
	[vin_code] [nvarchar](510) NULL,
	[engine_number] [nvarchar](510) NULL,
	[body_number] [nvarchar](510) NULL,
	[chassi_number] [nvarchar](510) NULL,
	[registration_type] [nvarchar](510) NULL,
	[type_of_owner] [nvarchar](510) NULL,
	[district_for_moscow] [nvarchar](510) NULL,
	[district_in_the_region] [nvarchar](510) NULL,
	[district_in_the_city_or_town] [nvarchar](510) NULL,
	[city_or_town] [nvarchar](510) NULL,
	[settlement] [nvarchar](510) NULL,
	[okato] [nvarchar](510) NULL,
	[oktmo] [nvarchar](510) NULL,
	[regional_center_or_region] [nvarchar](510) NULL,
	[street] [nvarchar](510) NULL,
	[code_for_streets] [nvarchar](510) NULL,
	[post_code] [nvarchar](510) NULL,
	[engine_power] [nvarchar](510) NULL,
	[engine_type] [nvarchar](510) NULL,
	[fuel_type] [nvarchar](510) NULL,
	[engine_displacement] [nvarchar](510) NULL,
	[engine_displacement_range] [nvarchar](510) NULL,
	[steering_wheel] [nvarchar](510) NULL,
	[body_type] [nvarchar](510) NULL,
	[gross_vehicle_weight] [nvarchar](510) NULL,
	[curb_weight] [nvarchar](510) NULL,
	[gvw_range_1] [nvarchar](510) NULL,
	[gvw_range_2] [nvarchar](510) NULL,
	[gvw_rating] [nvarchar](510) NULL,
	[gvw_range_3] [nvarchar](510) NULL,
	[gvw_range_4] [nvarchar](510) NULL,
	[gvw_class] [nvarchar](510) NULL,
	[axle_configuration] [nvarchar](510) NULL,
	[country_of_brand_origin] [nvarchar](510) NULL,
	[continent] [nvarchar](510) NULL,
	[domestic_or_foreign] [nvarchar](510) NULL,
	[price_segment] [nvarchar](510) NULL,
	[country_of_manufacture_vin_code] [nvarchar](510) NULL,
	[domestic_or_foreign_vin_code] [nvarchar](510) NULL,
	[manufacturer] [nvarchar](510) NULL,
	[country_of_brand_origin_chassis] [nvarchar](510) NULL,
	[domestic_or_foreign_chassis] [nvarchar](510) NULL,
	[customs_clearance_date] [nvarchar](510) NULL,
	[taxpayer_identification_number] [nvarchar](510) NULL,
	[company_name_tax_service] [nvarchar](1024) NULL,
	[okved_code_tax_service] [nvarchar](510) NULL,
	[company_name_gibdd] [nvarchar](510) NULL,
	[okved_decoding_tax_service] [nvarchar](510) NULL,
	[lcv] [nvarchar](510) NULL,
	[id] [nvarchar](510) NULL,
	[vins] [nvarchar](510) NULL,
	[file_name] [nvarchar](510) NULL,
	[date_of_loading] [datetime] NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_src_truck_registration_01] ON [dbo].[src_truck_registration]
(
	[record_id] ASC
)
INCLUDE([brand],[model_and_modification],[model],[modification],[year_of_manufacture],[vin_code],[engine_number],[body_number],[chassi_number],[engine_power],[engine_type],[fuel_type],[engine_displacement],[engine_displacement_range],[steering_wheel],[body_type],[gross_vehicle_weight],[curb_weight],[gvw_range_1],[gvw_range_2],[gvw_rating],[gvw_range_3],[gvw_range_4],[gvw_class],[axle_configuration],[country_of_manufacture_vin_code],[manufacturer],[country_of_brand_origin_chassis]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_src_truck_registration_02] ON [dbo].[src_truck_registration]
(
	[record_id] ASC
)
INCLUDE([federal_district],[economic_region],[region],[country_of_brand_origin],[continent],[domestic_or_foreign]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_src_truck_registration_03] ON [dbo].[src_truck_registration]
(
	[record_id] ASC
)
INCLUDE([taxpayer_identification_number],[company_name_tax_service]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_src_truck_registration_04] ON [dbo].[src_truck_registration]
(
	[record_id] ASC
)
INCLUDE([brand],[country_of_brand_origin],[continent]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_src_truck_registration_05] ON [dbo].[src_truck_registration]
(
	[record_id] ASC
)
INCLUDE([economic_region],[body_type]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_src_truck_registration_06] ON [dbo].[src_truck_registration]
(
	[record_id] ASC
)
INCLUDE([brand],[model_and_modification],[model],[modification]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_src_truck_registration_07] ON [dbo].[src_truck_registration]
(
	[record_id] ASC
)
INCLUDE([taxpayer_identification_number],[okved_code_tax_service]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_src_truck_registration_08] ON [dbo].[src_truck_registration]
(
	[record_id] ASC
)
INCLUDE([country_of_manufacture_vin_code],[country_of_brand_origin_chassis]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_src_truck_registration_09] ON [dbo].[src_truck_registration]
(
	[record_id] ASC
)
INCLUDE([year_of_registration],[axle_configuration]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_src_truck_registration_10] ON [dbo].[src_truck_registration]
(
	[record_id] ASC
)
INCLUDE([federal_district],[gvw_range_1]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
