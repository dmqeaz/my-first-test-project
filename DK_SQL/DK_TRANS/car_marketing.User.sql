USE [DK_TRANS]
GO
CREATE USER [car_marketing] FOR LOGIN [car_marketing] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_datareader] ADD MEMBER [car_marketing]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [car_marketing]
GO
