USE [DK_TRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[cars]
as 
select DISTINCT m.id as model_id, b.id as body_type_id, a.id as axle_type_id, w.id as weight_segment_id, 
s.vin_code, s.engine_number, s.body_number,s.chassi_number,
s.engine_type, s.fuel_type, s.engine_displacement, s.engine_displacement_range,
s.steering_wheel, s.gross_vehicle_weight, s.curb_weight,
c1.id as country_of_manufacture_vin_code_id,
s.manufacturer,
c2.id as country_of_brand_origin_chassis_id,
year_of_manufacture,
s.engine_power,
s.gvw_range_2,
s.gvw_range_3,
s.gvw_range_4,
s.gvw_rating,
s.gvw_class
from DK_SRC.dbo.src_truck_registration s LEFT JOIN DK_TRANS.dbo.dict_brands t ON s.brand = t.brand_name
left join DK_TRANS.dbo.dict_models m ON	t.id = m.brand_id and s.model = m.model and s.modification = m.modification and s.model_and_modification = m.model_and_modification
left join DK_TRANS.dbo.dict_body_types b on s.body_type = b.name
left join DK_TRANS.dbo.dict_axle_types a on s.axle_configuration = a.axle_type_text
left join DK_TRANS.dbo.dict_weight_segments w on s.gvw_range_1 = w.segment_text 
left join DK_TRANS.dbo.dict_countries c1 on s.country_of_manufacture_vin_code = c1.country
left join DK_TRANS.dbo.dict_countries c2 on s.country_of_brand_origin_chassis = c2.country
GO
