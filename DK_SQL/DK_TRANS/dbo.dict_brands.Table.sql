USE [DK_TRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dict_brands](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[brand_name] [nvarchar](60) NULL,
	[country_original_id] [int] NULL,
	[brand_segment] [nvarchar](20) NULL,
 CONSTRAINT [dict_brands_pk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[dict_brands]  WITH CHECK ADD  CONSTRAINT [dict_brands_dict_countries_fk] FOREIGN KEY([country_original_id])
REFERENCES [dbo].[dict_countries] ([id])
GO
ALTER TABLE [dbo].[dict_brands] CHECK CONSTRAINT [dict_brands_dict_countries_fk]
GO
