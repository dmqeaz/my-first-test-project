USE [DK_TRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dict_countries](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[country] [nvarchar](40) NOT NULL,
	[continent_id] [int] NULL,
	[country_type] [nvarchar](20) NULL,
 CONSTRAINT [dict_countries_pk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[dict_countries]  WITH CHECK ADD  CONSTRAINT [dict_countries_dict_continents_FK_1] FOREIGN KEY([continent_id])
REFERENCES [dbo].[dict_continents] ([id])
GO
ALTER TABLE [dbo].[dict_countries] CHECK CONSTRAINT [dict_countries_dict_continents_FK_1]
GO
