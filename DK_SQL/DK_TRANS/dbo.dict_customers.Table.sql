USE [DK_TRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dict_customers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[customer_name] [nvarchar](510) NULL,
	[short_customer_name] [nvarchar](510) NULL,
	[adress] [nvarchar](510) NULL,
	[phone] [nvarchar](1022) NULL,
	[email] [nvarchar](2046) NULL,
	[site] [nvarchar](510) NULL,
	[okved_old_id] [bigint] NULL,
	[index_of_financial_risks] [int] NULL,
	[company_size] [nvarchar](40) NULL,
	[number_of_employees] [nvarchar](200) NULL,
	[revenue] [float] NULL,
	[gross_profitability] [float] NULL,
	[net_profit] [nvarchar](200) NULL,
	[ros] [int] NULL,
	[group_company_id] [int] NULL,
	[okved_new_id] [bigint] NULL,
	[unique_tin] [nvarchar](24) NULL,
	[previous_tins] [nvarchar](200) NULL,
	[original_tin] [nvarchar](20) NULL,
	[feature_tin] [nvarchar](200) NULL,
	[date_of_adding] [nvarchar](8) NULL,
	[lists] [nvarchar](1022) NULL,
	[correct_tin] [nvarchar](24) NULL,
	[contact_person] [nvarchar](200) NULL,
	[registration_number] [nvarchar](26) NULL,
	[lising] [nvarchar](2) NULL,
	[source] [nvarchar](100) NULL,
 CONSTRAINT [dict_customers_pk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [_dta_index_dict_customers_01] ON [dbo].[dict_customers]
(
	[unique_tin] ASC
)
INCLUDE([id],[okved_old_id],[group_company_id],[okved_new_id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_dict_customers_02] ON [dbo].[dict_customers]
(
	[group_company_id] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[dict_customers]  WITH CHECK ADD  CONSTRAINT [dict_customers_dict_company_groups_FK] FOREIGN KEY([group_company_id])
REFERENCES [dbo].[dict_company_groups] ([id])
GO
ALTER TABLE [dbo].[dict_customers] CHECK CONSTRAINT [dict_customers_dict_company_groups_FK]
GO
ALTER TABLE [dbo].[dict_customers]  WITH CHECK ADD  CONSTRAINT [dict_customers_dict_industries_FK_1] FOREIGN KEY([okved_old_id])
REFERENCES [dbo].[dict_industries] ([id])
GO
ALTER TABLE [dbo].[dict_customers] CHECK CONSTRAINT [dict_customers_dict_industries_FK_1]
GO
ALTER TABLE [dbo].[dict_customers]  WITH CHECK ADD  CONSTRAINT [dict_customers_dict_industries_FK_2] FOREIGN KEY([okved_new_id])
REFERENCES [dbo].[dict_industries] ([id])
GO
ALTER TABLE [dbo].[dict_customers] CHECK CONSTRAINT [dict_customers_dict_industries_FK_2]
GO
