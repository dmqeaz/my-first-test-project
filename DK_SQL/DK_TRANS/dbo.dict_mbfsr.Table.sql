USE [DK_TRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dict_mbfsr](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[mbfsr_text] [varchar](60) NULL
) ON [PRIMARY]
GO
