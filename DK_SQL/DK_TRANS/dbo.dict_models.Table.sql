USE [DK_TRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dict_models](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[brand_id] [int] NULL,
	[model] [nvarchar](100) NULL,
	[modification] [nvarchar](100) NULL,
	[model_and_modification] [nvarchar](100) NULL,
	[datasource] [nvarchar](50) NULL,
 CONSTRAINT [dict_models_pk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[dict_models]  WITH CHECK ADD  CONSTRAINT [dict_models_dict_brands_FK] FOREIGN KEY([brand_id])
REFERENCES [dbo].[dict_brands] ([id])
GO
ALTER TABLE [dbo].[dict_models] CHECK CONSTRAINT [dict_models_dict_brands_FK]
GO
