USE [DK_TRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dict_price_segment](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[price_segment_name] [nvarchar](60) NULL,
	[park_value_from] [int] NULL,
	[park_value_to] [int] NULL,
	[sales_value_from] [int] NULL,
	[sales_value_to] [int] NULL,
	[Retail_KAM_name] [varchar](120) NULL,
	[Retail_KAM_id] [int] NULL,
 CONSTRAINT [dict_price_segment_pk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
