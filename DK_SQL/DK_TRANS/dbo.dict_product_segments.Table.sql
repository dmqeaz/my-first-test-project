USE [DK_TRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dict_product_segments](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[seg_name] [nvarchar](60) NULL,
	[seg_name_old] [nvarchar](60) NULL,
	[segment_g4g] [nvarchar](60) NULL,
 CONSTRAINT [dict_prod_segments_pk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
