USE [DK_TRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dict_regions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](60) NULL,
	[correct_name] [nvarchar](60) NULL,
	[federal_district_id] [int] NULL,
	[economic_region_id] [int] NULL,
 CONSTRAINT [dict_region_pk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[dict_regions]  WITH CHECK ADD  CONSTRAINT [dict_region_dict_economic_region_fk] FOREIGN KEY([economic_region_id])
REFERENCES [dbo].[dict_economic_regions] ([id])
GO
ALTER TABLE [dbo].[dict_regions] CHECK CONSTRAINT [dict_region_dict_economic_region_fk]
GO
ALTER TABLE [dbo].[dict_regions]  WITH CHECK ADD  CONSTRAINT [dict_region_dict_federal_district_fk] FOREIGN KEY([federal_district_id])
REFERENCES [dbo].[dict_federal_districts] ([id])
GO
ALTER TABLE [dbo].[dict_regions] CHECK CONSTRAINT [dict_region_dict_federal_district_fk]
GO
