USE [DK_TRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dict_retail_kam](
	[id] [int] NOT NULL,
	[retail_kam_name] [nvarchar](60) NULL,
	[park_value_from] [int] NULL,
	[park_value_to] [int] NULL,
	[sales_value_from] [int] NULL,
	[sales_value_to] [int] NULL,
 CONSTRAINT [dict_retail_kam_pk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
