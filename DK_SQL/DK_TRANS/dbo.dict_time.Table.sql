USE [DK_TRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dict_time](
	[Date] [date] NOT NULL,
	[DateString] [varchar](10) NULL,
	[Day] [int] NULL,
	[DayofYear] [int] NULL,
	[DayofWeek] [int] NULL,
	[DayofWeekName] [varchar](10) NULL,
	[Week] [int] NULL,
	[Month] [int] NULL,
	[MonthName] [varchar](10) NULL,
	[Quarter] [int] NULL,
	[Year] [int] NULL,
	[IsWeekend] [bit] NULL,
	[IsLeapYear] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
