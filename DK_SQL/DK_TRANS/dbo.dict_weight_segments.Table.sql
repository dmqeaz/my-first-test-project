USE [DK_TRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dict_weight_segments](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[segment_text] [nvarchar](20) NULL,
	[value_from] [int] NULL,
	[value_to] [int] NULL,
 CONSTRAINT [dict_weight_segments_pk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
