USE [DK_TRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[map_models_prod_segments](
	[id] [bigint] NOT NULL,
	[date_from] [date] NOT NULL,
	[date_to] [date] NULL,
	[model_id] [int] NOT NULL,
	[body_type_id] [int] NULL,
	[weight_segment_id] [int] NULL,
	[axle_type_id] [int] NULL,
	[engine_power] [nvarchar](80) NULL,
	[version_id] [int] NOT NULL,
	[prod_segment_id] [int] NULL,
	[brand_id] [int] NULL,
	[map_type] [nvarchar](20) NULL,
 CONSTRAINT [map_models_prod_segments_pk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[map_models_prod_segments]  WITH CHECK ADD  CONSTRAINT [map_models_prod_segments_dict_axle_types_FK] FOREIGN KEY([axle_type_id])
REFERENCES [dbo].[dict_axle_types] ([id])
GO
ALTER TABLE [dbo].[map_models_prod_segments] CHECK CONSTRAINT [map_models_prod_segments_dict_axle_types_FK]
GO
ALTER TABLE [dbo].[map_models_prod_segments]  WITH CHECK ADD  CONSTRAINT [map_models_prod_segments_dict_body_types_FK] FOREIGN KEY([body_type_id])
REFERENCES [dbo].[dict_body_types] ([id])
GO
ALTER TABLE [dbo].[map_models_prod_segments] CHECK CONSTRAINT [map_models_prod_segments_dict_body_types_FK]
GO
ALTER TABLE [dbo].[map_models_prod_segments]  WITH CHECK ADD  CONSTRAINT [map_models_prod_segments_dict_brands_FK] FOREIGN KEY([brand_id])
REFERENCES [dbo].[dict_brands] ([id])
GO
ALTER TABLE [dbo].[map_models_prod_segments] CHECK CONSTRAINT [map_models_prod_segments_dict_brands_FK]
GO
ALTER TABLE [dbo].[map_models_prod_segments]  WITH CHECK ADD  CONSTRAINT [map_models_prod_segments_dict_models_FK] FOREIGN KEY([model_id])
REFERENCES [dbo].[dict_models] ([id])
GO
ALTER TABLE [dbo].[map_models_prod_segments] CHECK CONSTRAINT [map_models_prod_segments_dict_models_FK]
GO
ALTER TABLE [dbo].[map_models_prod_segments]  WITH CHECK ADD  CONSTRAINT [map_models_prod_segments_dict_versions_FK] FOREIGN KEY([version_id])
REFERENCES [dbo].[dict_versions] ([id])
GO
ALTER TABLE [dbo].[map_models_prod_segments] CHECK CONSTRAINT [map_models_prod_segments_dict_versions_FK]
GO
ALTER TABLE [dbo].[map_models_prod_segments]  WITH CHECK ADD  CONSTRAINT [map_models_prod_segments_dict_weight_segments_FK] FOREIGN KEY([weight_segment_id])
REFERENCES [dbo].[dict_weight_segments] ([id])
GO
ALTER TABLE [dbo].[map_models_prod_segments] CHECK CONSTRAINT [map_models_prod_segments_dict_weight_segments_FK]
GO
