USE [DK_TRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[registrations](
	[id] [bigint] NOT NULL,
	[region_id] [int] NULL,
	[time_id] [date] NULL,
	[company_id] [int] NULL,
	[model_id] [int] NULL,
	[body_type_id] [int] NULL,
	[axle_id] [int] NULL,
	[weight_segment_id] [int] NULL,
	[vin_code] [nvarchar](40) NULL,
	[engine_number] [nvarchar](40) NULL,
	[body_number] [nvarchar](40) NULL,
	[chassi_number] [nvarchar](40) NULL,
	[new_or_used] [nvarchar](40) NULL,
	[registration_type] [nvarchar](60) NULL,
	[type_of_owner] [nvarchar](40) NULL,
	[district_for_moscow] [nvarchar](120) NULL,
	[district_in_the_region] [nvarchar](120) NULL,
	[district_in_the_city_or_town] [nvarchar](120) NULL,
	[city_or_town] [nvarchar](120) NULL,
	[settlement] [nvarchar](120) NULL,
	[okato] [nvarchar](40) NULL,
	[oktmo] [nvarchar](40) NULL,
	[regional_center_or_region] [nvarchar](120) NULL,
	[street] [nvarchar](120) NULL,
	[code_for_streets] [nvarchar](400) NULL,
	[post_code] [nvarchar](40) NULL,
	[lcv] [nvarchar](10) NULL,
	[mbfsr] [nvarchar](4) NULL,
	[engine_type] [nvarchar](40) NULL,
	[fuel_type] [nvarchar](40) NULL,
	[engine_displacement] [int] NULL,
	[engine_displacement_range] [nvarchar](40) NULL,
	[steering_wheel] [nvarchar](40) NULL,
	[gross_vehicle_weight] [nvarchar](40) NULL,
	[curb_weight] [nvarchar](40) NULL,
	[country_of_manufacture_vin_code_id] [int] NULL,
	[manufacturer] [nvarchar](400) NULL,
	[country_of_brand_origin_chassis_id] [int] NULL,
	[year_of_manufacture] [nvarchar](8) NULL,
	[engine_power] [int] NULL,
	[gvw_range_2] [nvarchar](40) NULL,
	[gvw_range_3] [nvarchar](40) NULL,
	[gvw_range_4] [nvarchar](40) NULL,
	[gvw_rating] [nvarchar](40) NULL,
	[gvw_class] [nvarchar](40) NULL,
 CONSTRAINT [registrations_pk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_registrations_01] ON [dbo].[registrations]
(
	[id] ASC
)
INCLUDE([time_id],[company_id],[model_id],[vin_code],[chassi_number]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[registrations]  WITH CHECK ADD  CONSTRAINT [dict_cars_dict_axle_types_FK_2] FOREIGN KEY([axle_id])
REFERENCES [dbo].[dict_axle_types] ([id])
GO
ALTER TABLE [dbo].[registrations] CHECK CONSTRAINT [dict_cars_dict_axle_types_FK_2]
GO
ALTER TABLE [dbo].[registrations]  WITH CHECK ADD  CONSTRAINT [dict_cars_dict_body_types_FK_2] FOREIGN KEY([body_type_id])
REFERENCES [dbo].[dict_body_types] ([id])
GO
ALTER TABLE [dbo].[registrations] CHECK CONSTRAINT [dict_cars_dict_body_types_FK_2]
GO
ALTER TABLE [dbo].[registrations]  WITH CHECK ADD  CONSTRAINT [dict_cars_dict_countries_FK_2] FOREIGN KEY([country_of_manufacture_vin_code_id])
REFERENCES [dbo].[dict_countries] ([id])
GO
ALTER TABLE [dbo].[registrations] CHECK CONSTRAINT [dict_cars_dict_countries_FK_2]
GO
ALTER TABLE [dbo].[registrations]  WITH CHECK ADD  CONSTRAINT [dict_cars_dict_countries_FK_3] FOREIGN KEY([country_of_brand_origin_chassis_id])
REFERENCES [dbo].[dict_countries] ([id])
GO
ALTER TABLE [dbo].[registrations] CHECK CONSTRAINT [dict_cars_dict_countries_FK_3]
GO
ALTER TABLE [dbo].[registrations]  WITH CHECK ADD  CONSTRAINT [registrations_dict_customers_FK] FOREIGN KEY([company_id])
REFERENCES [dbo].[dict_customers] ([id])
GO
ALTER TABLE [dbo].[registrations] CHECK CONSTRAINT [registrations_dict_customers_FK]
GO
ALTER TABLE [dbo].[registrations]  WITH CHECK ADD  CONSTRAINT [registrations_dict_models_FK] FOREIGN KEY([model_id])
REFERENCES [dbo].[dict_models] ([id])
GO
ALTER TABLE [dbo].[registrations] CHECK CONSTRAINT [registrations_dict_models_FK]
GO
ALTER TABLE [dbo].[registrations]  WITH CHECK ADD  CONSTRAINT [registrations_dict_regions_FK] FOREIGN KEY([region_id])
REFERENCES [dbo].[dict_regions] ([id])
GO
ALTER TABLE [dbo].[registrations] CHECK CONSTRAINT [registrations_dict_regions_FK]
GO
ALTER TABLE [dbo].[registrations]  WITH CHECK ADD  CONSTRAINT [registrations_dict_time_FK] FOREIGN KEY([time_id])
REFERENCES [dbo].[dict_time] ([Date])
GO
ALTER TABLE [dbo].[registrations] CHECK CONSTRAINT [registrations_dict_time_FK]
GO
ALTER TABLE [dbo].[registrations]  WITH CHECK ADD  CONSTRAINT [registrations_dict_weight_segments_FK] FOREIGN KEY([weight_segment_id])
REFERENCES [dbo].[dict_weight_segments] ([id])
GO
ALTER TABLE [dbo].[registrations] CHECK CONSTRAINT [registrations_dict_weight_segments_FK]
GO
