USE [DK_TRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tran_company_parks](
	[date_id] [date] NOT NULL,
	[customer_id] [int] NOT NULL,
	[park] [int] NULL,
	[year_sales] [int] NULL,
 CONSTRAINT [tran_company_parks_pk] PRIMARY KEY CLUSTERED 
(
	[date_id] ASC,
	[customer_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tran_company_parks]  WITH CHECK ADD  CONSTRAINT [tran_company_parks_dict_customers_FK] FOREIGN KEY([customer_id])
REFERENCES [dbo].[dict_customers] ([id])
GO
ALTER TABLE [dbo].[tran_company_parks] CHECK CONSTRAINT [tran_company_parks_dict_customers_FK]
GO
ALTER TABLE [dbo].[tran_company_parks]  WITH CHECK ADD  CONSTRAINT [tran_company_parks_dict_time_fk] FOREIGN KEY([date_id])
REFERENCES [dbo].[dict_time] ([Date])
GO
ALTER TABLE [dbo].[tran_company_parks] CHECK CONSTRAINT [tran_company_parks_dict_time_fk]
GO
