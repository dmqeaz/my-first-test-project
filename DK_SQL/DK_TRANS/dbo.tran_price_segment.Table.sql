USE [DK_TRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tran_price_segment](
	[customer_id] [int] NOT NULL,
	[date_id] [date] NOT NULL,
	[price_segment_id] [int] NULL,
	[rule] [nvarchar](60) NULL,
 CONSTRAINT [tran_price_segment_pk] PRIMARY KEY CLUSTERED 
(
	[customer_id] ASC,
	[date_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tran_price_segment]  WITH CHECK ADD  CONSTRAINT [tran_price_segment_dict_customers_FK] FOREIGN KEY([customer_id])
REFERENCES [dbo].[dict_customers] ([id])
GO
ALTER TABLE [dbo].[tran_price_segment] CHECK CONSTRAINT [tran_price_segment_dict_customers_FK]
GO
ALTER TABLE [dbo].[tran_price_segment]  WITH CHECK ADD  CONSTRAINT [tran_price_segment_dict_price_segment_fk] FOREIGN KEY([price_segment_id])
REFERENCES [dbo].[dict_price_segment] ([id])
GO
ALTER TABLE [dbo].[tran_price_segment] CHECK CONSTRAINT [tran_price_segment_dict_price_segment_fk]
GO
ALTER TABLE [dbo].[tran_price_segment]  WITH CHECK ADD  CONSTRAINT [tran_price_segment_dict_time_fk] FOREIGN KEY([date_id])
REFERENCES [dbo].[dict_time] ([Date])
GO
ALTER TABLE [dbo].[tran_price_segment] CHECK CONSTRAINT [tran_price_segment_dict_time_fk]
GO
