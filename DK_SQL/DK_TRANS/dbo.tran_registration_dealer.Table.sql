USE [DK_TRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tran_registration_dealer](
	[reg_id] [bigint] NOT NULL,
	[time_id] [date] NOT NULL,
	[dealer_id] [int] NULL,
 CONSTRAINT [tran_registration_dealer_pk] PRIMARY KEY CLUSTERED 
(
	[reg_id] ASC,
	[time_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tran_registration_dealer]  WITH CHECK ADD  CONSTRAINT [tran_registration_dealer_dict_dealers_fk] FOREIGN KEY([dealer_id])
REFERENCES [dbo].[dict_dealers] ([id])
GO
ALTER TABLE [dbo].[tran_registration_dealer] CHECK CONSTRAINT [tran_registration_dealer_dict_dealers_fk]
GO
ALTER TABLE [dbo].[tran_registration_dealer]  WITH CHECK ADD  CONSTRAINT [tran_registration_dealer_registrations_fk] FOREIGN KEY([reg_id])
REFERENCES [dbo].[registrations] ([id])
GO
ALTER TABLE [dbo].[tran_registration_dealer] CHECK CONSTRAINT [tran_registration_dealer_registrations_fk]
GO
