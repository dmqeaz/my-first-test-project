USE [DK_TRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tran_registration_prod_segment](
	[reg_id] [bigint] NOT NULL,
	[version_id] [int] NOT NULL,
	[time_id] [date] NULL,
	[segment_id] [int] NULL,
 CONSTRAINT [tran_registration_segment_pk] PRIMARY KEY CLUSTERED 
(
	[reg_id] ASC,
	[version_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tran_registration_prod_segment]  WITH CHECK ADD  CONSTRAINT [tran_registration_prod_segment_dict_prod_segments_fk] FOREIGN KEY([segment_id])
REFERENCES [dbo].[dict_product_segments] ([id])
GO
ALTER TABLE [dbo].[tran_registration_prod_segment] CHECK CONSTRAINT [tran_registration_prod_segment_dict_prod_segments_fk]
GO
ALTER TABLE [dbo].[tran_registration_prod_segment]  WITH CHECK ADD  CONSTRAINT [tran_registration_prod_segment_dict_versions_fk] FOREIGN KEY([version_id])
REFERENCES [dbo].[dict_versions] ([id])
GO
ALTER TABLE [dbo].[tran_registration_prod_segment] CHECK CONSTRAINT [tran_registration_prod_segment_dict_versions_fk]
GO
ALTER TABLE [dbo].[tran_registration_prod_segment]  WITH CHECK ADD  CONSTRAINT [tran_registration_prod_segment_registrations_fk] FOREIGN KEY([reg_id])
REFERENCES [dbo].[registrations] ([id])
GO
ALTER TABLE [dbo].[tran_registration_prod_segment] CHECK CONSTRAINT [tran_registration_prod_segment_registrations_fk]
GO
