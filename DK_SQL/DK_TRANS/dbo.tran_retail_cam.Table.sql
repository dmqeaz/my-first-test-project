USE [DK_TRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tran_retail_cam](
	[reg_id] [bigint] NOT NULL,
	[date_id] [date] NOT NULL,
	[retail_kam_id] [int] NOT NULL,
	[rule] [nvarchar](40) NULL,
	[customer_id] [int] NULL,
 CONSTRAINT [tran_retail_cam_pk] PRIMARY KEY CLUSTERED 
(
	[reg_id] ASC,
	[retail_kam_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tran_retail_cam]  WITH CHECK ADD  CONSTRAINT [tran_retail_cam_dict_customers_FK] FOREIGN KEY([customer_id])
REFERENCES [dbo].[dict_customers] ([id])
GO
ALTER TABLE [dbo].[tran_retail_cam] CHECK CONSTRAINT [tran_retail_cam_dict_customers_FK]
GO
ALTER TABLE [dbo].[tran_retail_cam]  WITH CHECK ADD  CONSTRAINT [tran_retail_cam_dict_retail_kam_FK] FOREIGN KEY([retail_kam_id])
REFERENCES [dbo].[dict_retail_kam] ([id])
GO
ALTER TABLE [dbo].[tran_retail_cam] CHECK CONSTRAINT [tran_retail_cam_dict_retail_kam_FK]
GO
ALTER TABLE [dbo].[tran_retail_cam]  WITH CHECK ADD  CONSTRAINT [tran_retail_cam_dict_time_FK] FOREIGN KEY([date_id])
REFERENCES [dbo].[dict_time] ([Date])
GO
ALTER TABLE [dbo].[tran_retail_cam] CHECK CONSTRAINT [tran_retail_cam_dict_time_FK]
GO
ALTER TABLE [dbo].[tran_retail_cam]  WITH CHECK ADD  CONSTRAINT [tran_retail_cam_registrations_FK] FOREIGN KEY([reg_id])
REFERENCES [dbo].[registrations] ([id])
GO
ALTER TABLE [dbo].[tran_retail_cam] CHECK CONSTRAINT [tran_retail_cam_registrations_FK]
GO
