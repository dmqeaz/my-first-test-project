USE [DK_TRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_dim_brand_loader]
AS SELECT DK_TRANS.dbo.dict_brands.id,
    DK_TRANS.dbo.dict_brands.brand_name,
    DK_TRANS.dbo.dict_countries.country,
    DK_TRANS.dbo.dict_countries.country_type,
    DK_TRANS.dbo.dict_continents.name,
    DK_TRANS.dbo.dict_brands.brand_segment
   FROM DK_TRANS.dbo.dict_brands
     LEFT JOIN DK_TRANS.dbo.dict_countries ON DK_TRANS.dbo.dict_brands.country_original_id = DK_TRANS.dbo.dict_countries.id
     LEFT JOIN DK_TRANS.dbo.dict_continents ON DK_TRANS.dbo.dict_countries.continent_id = DK_TRANS.dbo.dict_continents.id;
GO
