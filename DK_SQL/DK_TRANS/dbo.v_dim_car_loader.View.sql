USE [DK_TRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_dim_car_loader]
AS SELECT 
r.id as car_id, 
r.vin_code, 
r.engine_number, 
r.body_number,
r.year_of_manufacture,
r.fuel_type, 
r.engine_displacement, 
r.engine_displacement_range, 
r.steering_wheel, 
r.gross_vehicle_weight, 
r.curb_weight, 
mcount.country AS country_of_manufacture, 
r.manufacturer, 
chcount.country AS country_of_brand_origin_chassis,
r.chassi_number,  
r.engine_type,   
r.gvw_range_2, 
r.gvw_range_3, 
r.gvw_range_4, 
r.gvw_rating, 
r.gvw_class
FROM DK_TRANS.dbo.registrations r
LEFT JOIN DK_TRANS.dbo.dict_countries mcount ON r.country_of_manufacture_vin_code_id = mcount.id
     LEFT JOIN DK_TRANS.dbo.dict_countries chcount ON r.country_of_manufacture_vin_code_id = chcount.id;
GO
