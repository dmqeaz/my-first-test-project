USE [DK_TRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_dim_customer_loader]
AS 
SELECT 
	c.id,
    c.unique_tin,
    c.correct_tin,
    c.customer_name,
    c.short_customer_name,
    okved_new.okved AS okved_code_new,
    okved_new.industry_name AS okved_name_new,
    okved_old.okved AS okved_code_old,
    okved_old.industry_name AS okved_name_old,
	c.company_size,
    c.revenue,
    c.number_of_employees,
    c.net_profit,
    c.gross_profitability,
    c.ros,
    c.index_of_financial_risks,
    c.registration_number,
    c.contact_person,
    c.adress,
    c.phone,
    c.email,
    c.site
   FROM DK_TRANS.dbo.dict_customers c
     LEFT JOIN DK_TRANS.dbo.dict_industries okved_new ON c.okved_new_id = okved_new.id
	 LEFT JOIN DK_TRANS.dbo.dict_industries okved_old ON c.okved_new_id = okved_old.id
--     LEFT JOIN dk.dict_company_sizes sizes ON dict_companies.company_size_id = sizes.id;
GO
