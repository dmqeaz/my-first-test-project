USE [DK_TRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_dim_region_loader]
AS SELECT DK_TRANS.dbo.dict_regions.id,
    DK_TRANS.dbo.dict_regions.name,
    DK_TRANS.dbo.dict_regions.correct_name,
    DK_TRANS.dbo.dict_federal_districts.name AS federal_district,
    DK_TRANS.dbo.dict_federal_districts.correct_name AS fd_correct_name,
    DK_TRANS.dbo.dict_economic_regions.name AS economic_region
   FROM DK_TRANS.dbo.dict_regions
     LEFT JOIN DK_TRANS.dbo.dict_federal_districts ON DK_TRANS.dbo.dict_regions.federal_district_id = DK_TRANS.dbo.dict_federal_districts.id
     LEFT JOIN DK_TRANS.dbo.dict_economic_regions ON DK_TRANS.dbo.dict_regions.economic_region_id = DK_TRANS.dbo.dict_economic_regions.id;
GO
