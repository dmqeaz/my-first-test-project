USE [DK_TRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_dim_registration_loader]
AS SELECT 
id as reg_id, 
new_or_used, 
registration_type, 
type_of_owner, 
district_for_moscow, 
district_in_the_region, 
district_in_the_city_or_town, 
city_or_town, 
settlement, 
okato, 
oktmo, 
regional_center_or_region, 
street, 
code_for_streets, 
post_code, 
lcv 
FROM DK_TRANS.dbo.registrations;
GO
