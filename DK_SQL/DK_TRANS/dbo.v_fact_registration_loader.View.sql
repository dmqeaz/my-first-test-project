USE [DK_TRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_fact_registration_loader] AS
SELECT
    r.time_id,
    m.brand_id,
    r.region_id,
    r.model_id,
    r.id as reg_id,
    COALESCE(dealer.dealer_id,0) AS dealer_id,
    COALESCE(pr_seg.segment_id,0) AS product_segment_id,
    COALESCE(price_seg.price_segment_id,0) as price_segment_id,
    COALESCE(rc.retail_kam_id,0) as retail_kam_id,
    5 as version_id,
    r.id as car_id,
    COALESCE(r.company_id,0) as company_id,
    1 as quantity,
    0 as mbfsr_id,
    COALESCE(cust.group_company_id,0) as company_group_id,
    0 as error_code,
    r.axle_id,
    r.body_type_id,
    r.weight_segment_id
FROM DK_TRANS.dbo.registrations r
LEFT JOIN DK_TRANS.dbo.dict_models m
    ON r.model_id = m.id
LEFT JOIN DK_TRANS.dbo.tran_registration_dealer dealer
    ON r.id = dealer.reg_id
LEFT JOIN DK_TRANS.dbo.tran_registration_prod_segment pr_seg
    ON r.id = pr_seg.reg_id
LEFT JOIN DK_TRANS.dbo.dict_customers cust
    ON r.company_id = cust.id
LEFT JOIN DK_TRANS.dbo.tran_price_segment price_seg
    ON r.company_id = price_seg.customer_id
   AND YEAR(r.time_id)= YEAR(price_seg.date_id)
LEFT JOIN DK_TRANS.dbo.tran_retail_cam rc
    ON r.id = rc.reg_id
GO
