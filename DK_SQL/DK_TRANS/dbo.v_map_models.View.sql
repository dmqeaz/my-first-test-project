USE [DK_TRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_map_models] as
SELECT h.record_id,
h.date_of_loading,
m.id as model_id,
t.id as body_type_id,
w.id as weight_segment_id,
a.id   as axle_type_id,
h.engine_power,
p.id as prod_segment_id,
b.id as brand_id
FROM DK_SRC.dbo.src_h_segments_product_competitors_clear h
left join DK_TRANS.dbo.dict_brands b on b.brand_name = h.brand 
left join DK_TRANS.dbo.dict_body_types t on t.name = h.body_type
left join DK_TRANS.dbo.dict_models m on m.model_and_modification = h.model_and_modification and m.brand_id = b.id
left join DK_TRANS.dbo.dict_axle_types a on a.axle_type_text = h.axle_configuration
left join DK_TRANS.dbo.dict_weight_segments w on w.segment_text = h.weight_in_a_segment_1
left OUTER join DK_TRANS.dbo.dict_product_segments p on ISNULL (p.seg_name_old, 0) = ISNULL (h.segments, 0) and ISNULL (p.seg_name, 0) = ISNULL (h.new_segment, 0)
GO
