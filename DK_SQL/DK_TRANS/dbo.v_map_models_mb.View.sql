USE [DK_TRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_map_models_mb] AS
SELECT  
m.record_id, 
b.id as brand_id, 
m.model_and_modification, 
md.id as model_id, 
m.segment_name, 
s.id segment_id, 
m.date_of_loading, 
m.filename 
FROM DK_SRC.dbo.src_h_segments_mersedes m
left join DK_TRANS.dbo.dict_brands b on 
b.brand_name = 'MERCEDES-BENZ'
left join DK_TRANS.dbo.dict_models md on
md.model_and_modification = m.model_and_modification
left join DK_TRANS.dbo.dict_product_segments s on
s.seg_name = m.segment_name and s.seg_name_old is null
GO
