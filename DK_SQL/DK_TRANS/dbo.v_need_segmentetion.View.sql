USE [DK_TRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_need_segmentetion] AS
SELECT 
	reg.id,
    reg.time_id,
    reg.axle_id,
    reg.body_type_id,
    reg.weight_segment_id,
    m.brand_id,
    reg.model_id,
    reg.vin_code,
    reg.chassi_number,
    reg.body_number,
    reg.engine_number,
    reg.engine_power,
    vers.id as vers_id
   FROM DK_TRANS.dbo.registrations reg
     JOIN ( SELECT reg_1.id
           FROM DK_TRANS.dbo.registrations reg_1
        EXCEPT
         SELECT tran_registration_prod_segment.reg_id AS id
           FROM DK_TRANS.dbo.tran_registration_prod_segment
 ) need_segmentation ON reg.id = need_segmentation.id
 	left join DK_TRANS.dbo.dict_models m on reg.model_id = m.id
    CROSS join DK_TRANS.dbo.dict_versions vers
GO
