USE [DK_TRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_reg_customer] AS
SELECT
    c.id,
    parks.date_id,
    parks.park,
    COALESCE(sales.year_sales, 0) AS year_sales,
    r_park.id AS park_retail_kam_id,
    r_park.retail_kam_name as park_retail_kam_name,
    r_sales.id AS sales_retail_kam_id,
    r_sales.retail_kam_name AS sales_retail_kam_name
FROM DK_TRANS.dbo.dict_customers c
LEFT JOIN (
    SELECT
        date_id,
        customer_id,
        park
    FROM DK_TRANS.dbo.tran_company_parks
          ) AS parks
 ON c.id = parks.customer_id
LEFT JOIN (
    SELECT
        CAST(datename(year, time_id) + '-01-01' as datetime) as date_id,
        company_id,
        COUNT(*) AS year_sales
    FROM DK_TRANS.dbo.registrations
    WHERE company_id IS NOT NULL
GROUP BY cast(datename(year,time_id) + '-01-01' as datetime), company_id
          ) AS sales
 ON c.id = sales.company_id
AND YEAR (parks.date_id) = YEAR (sales.date_id)
LEFT JOIN DK_TRANS.dbo.dict_retail_kam r_park
 ON parks.park >= r_park.park_value_from
AND parks.park <= r_park.park_value_to
LEFT JOIN DK_TRANS.dbo.dict_retail_kam r_sales
 ON COALESCE(sales.year_sales, 0) >= r_sales.sales_value_from
AND COALESCE(sales.year_sales, 0) <= r_sales.sales_value_to;
GO
