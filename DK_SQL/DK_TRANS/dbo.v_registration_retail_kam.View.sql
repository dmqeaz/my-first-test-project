USE [DK_TRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_registration_retail_kam]
AS 
SELECT 
reg.id as reg_id,
    reg.time_id,
    reg.company_id,
    reg.vin_code,
    reg.chassi_number, 
    reg.type_of_owner
   FROM DK_TRANS.dbo.registrations reg
     JOIN ( SELECT reg_1.id
           FROM DK_TRANS.dbo.registrations reg_1
        EXCEPT
         SELECT tran_retail_cam.reg_id AS id
           FROM DK_TRANS.dbo.tran_retail_cam ) need_segmentation ON reg.id = need_segmentation.id;
GO
