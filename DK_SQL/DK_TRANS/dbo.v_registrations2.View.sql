USE [DK_TRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_registrations2] AS
SELECT
    s.record_id,
    r.id AS region_id,
    t.[Date] AS date_id,
    cust.id AS company_id,
    b.id AS brand_id,
    m.id AS model_id,
    bo.id AS body_type_id,
    a.id AS axle_type_id,
    w.id AS weight_segment_id,
    s.vin_code,
    s.engine_number,
    s.body_number,
    s.chassi_number,
    s.new_or_used,
    s.registration_type,
    s.type_of_owner,
    s.district_for_moscow,
    s.district_in_the_region,
    s.district_in_the_city_or_town,
    s.city_or_town,
    s.settlement,
    s.okato,
    s.oktmo,
    s.regional_center_or_region,
    s.street,
    s.code_for_streets,
    s.post_code,
    s.lcv,
    s.engine_type,
    s.fuel_type,
    TRY_CAST(s.engine_displacement AS INT) AS engine_displacement,
    s.engine_displacement_range,
    s.steering_wheel,
    s.gross_vehicle_weight,
    s.curb_weight,
    c1.id AS country_of_manufacture_vin_code_id,
    s.manufacturer,
    c2.id AS country_of_brand_origin_chassis_id,
    s.year_of_manufacture,
    TRY_CAST(s.engine_power AS INT) AS engine_power,
    s.gvw_range_2,
    s.gvw_rating,
    s.gvw_range_3,
    s.gvw_range_4,
    s.gvw_class
FROM DK_SRC.dbo.src_truck_registration s
LEFT JOIN DK_TRANS.dbo.dict_regions r
    ON s.region = r.name
LEFT JOIN DK_TRANS.dbo.dict_time t
    ON s.year_of_registration + s.month_of_registration + s.day_of_registration = t.[Date]
LEFT JOIN DK_TRANS.dbo.dict_customers cust
    ON s.taxpayer_identification_number = cust.unique_tin
LEFT JOIN DK_TRANS.dbo.dict_brands b
    ON s.brand = b.brand_name
LEFT JOIN DK_TRANS.dbo.dict_models m
    ON b.id = m.brand_id
   AND s.model_and_modification = m.model_and_modification
LEFT JOIN DK_TRANS.dbo.dict_body_types bo
    ON s.body_type = bo.name
LEFT JOIN DK_TRANS.dbo.dict_axle_types a
    ON a.axle_type_text = s.axle_configuration
LEFT JOIN DK_TRANS.dbo.dict_countries c1
    ON s.country_of_manufacture_vin_code = c1.country
LEFT JOIN DK_TRANS.dbo.dict_countries c2
    ON s.country_of_brand_origin_chassis = c2.country
LEFT JOIN DK_TRANS.dbo.dict_weight_segments w
    ON s.gvw_range_1 = w.segment_text
WHERE s.brand IN ('VOLVO','RENAULT','MAN','MERCEDES-BENZ','IVECO','DAF','SCANIA')
  AND s.new_or_used = 'new'
  AND s.gvw_range_1 NOT IN ('3500','3501-5000','5001-6000')
  AND NOT (s.brand = 'IVECO'
       AND s.model = 'Daily'
          )
  AND NOT (s.brand = 'MERCEDES-BENZ'
       AND s.model IN ('SPRINTER','VITO','VIANO')
          )
GO
