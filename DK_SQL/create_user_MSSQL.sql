USE DK_TRANS;
DROP USER car_marketing;
GO;

USE master;
DROP LOGIN car_marketing;
GO;

CREATE LOGIN car_marketing WITH PASSWORD = N'XXXXXX', DEFAULT_DATABASE=DK_TRANS, CHECK_EXPIRATION=OFF, CHECK_POLICY=ON;
GO;

DENY VIEW ANY DATABASE TO car_marketing;
DENY VIEW SERVER STATE TO car_marketing;
GO;

USE DK_TRANS;
CREATE USER car_marketing FOR LOGIN car_marketing WITH DEFAULT_SCHEMA=dbo;
EXEC sp_addrolemember N'db_datareader', N'car_marketing';
EXEC sp_addrolemember N'db_datawriter', N'car_marketing';
GO;
GRANT EXECUTE ON SCHEMA ::dbo TO car_marketing;
GO;
