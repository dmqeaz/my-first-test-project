FROM node:11-alpine
USER root

#build client 
WORKDIR /usr/src/app/client
COPY angular .
RUN npm install @angular/cli &&\
    npm run build -- --prod &&\
    mv ./public ../public  &&\
    rm -rf /usr/src/app/client
    
#Build server
WORKDIR /usr/src/app
COPY nodejs .

#Set env
ENV JAVA_HOME /usr/lib/jvm/java-1.8-openjdk
ENV PATH=$PATH:${JAVA_HOME}/bin

# install dependencies including JDK, build server, replace JDK with JRE to reduce image size
RUN apk --update add nss-dev  &&\
    apk --update add --virtual build-dependencies build-base python2 openjdk8  &&\
#   npm install express  &&\
    npm install --python=python2.7 &&\
    npm prune --production    &&\
    apk del build-dependencies    &&\
    apk --update add openjdk8-jre

#check env
#CMD ["java", "-version"]
#RUN which java
#RUN echo $JAVA_HOME
#RUN echo $PATH

EXPOSE 3000
CMD [ "npm", "start" ]