import {Component, OnInit} from '@angular/core';
import {MatDialog, MatDialogConfig, MatIconRegistry} from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';
import {ActivatedRoute} from '@angular/router';
import {AuthDialogComponent} from './auth-dialog/auth-dialog.component';
import {CookieService} from './cookie.service';
import {DataProviderService} from './data-provider.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'data-manager';

  public isAuthComplete = false;
  public selectedTableName: string;
  public isFullMode = false;
  public currentState: string;

  constructor(private matIconRegistry: MatIconRegistry,
              private domSanitizer: DomSanitizer,
              private activatedRoute: ActivatedRoute,
              private dialog: MatDialog,
              private dataProviderService: DataProviderService) {
    const externalIcons = ['file_excel', 'add_filter'];
    externalIcons.forEach((icon) => {
      this.matIconRegistry.addSvgIcon(
        icon,
        this.domSanitizer.bypassSecurityTrustResourceUrl(`../assets/` + icon + `.svg`)
      );
    });

    this.activatedRoute.queryParams.subscribe(params => {
      this.selectedTableName = params['table'] ? params['table'] + '.json' : undefined;
      if (params['table']) {
        window.history.replaceState({}, document.title, '/');
        this.isFullMode = true;
      }
      this.updateCurrentState();
    });
  }

  ngOnInit() {
  }

  updateCurrentState() {
    if (!this.isAuthComplete) {
      if (this.currentState !== 'auth') {
        this.currentState = 'auth';

        const savedLogin = CookieService.getCookie('TableEditingToolUsername');
        const savedToken = CookieService.getCookie('TableEditingToolToken');

        const config = new MatDialogConfig();
        config.hasBackdrop = false;
        config.disableClose = true;
        config.autoFocus = true;

        const openAuthDialog = () => {
          this.dialog.open(AuthDialogComponent, config)
            .afterClosed()
            .subscribe(() => {
              this.onAuthComplete();
            });
        };

        if (savedLogin && savedToken) {
          this.checkToken(savedLogin, savedToken).subscribe((result) => {
            if (result.isTokenValid) {
              this.onAuthComplete();
            } else {
              openAuthDialog();
            }
          });
        } else {
          openAuthDialog();
        }
      }
    } else {
      if (this.currentState !== 'showData') {
        this.currentState = 'showData';
      }
    }
  }

  checkToken(login, token) {
    return this.dataProviderService.checkToken(login, token);
  }

  onAuthComplete() {
    this.isAuthComplete = true;
    this.updateCurrentState();
  }

}
