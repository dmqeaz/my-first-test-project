import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {MDBBootstrapModule} from 'angular-bootstrap-md';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MAT_DIALOG_DATA,
  MatAutocompleteModule, MatBadgeModule, MatBottomSheetModule, MatButtonModule, MatButtonToggleModule, MatCardModule,
  MatCheckboxModule, MatChipsModule, MatDatepickerModule, MatDialogModule, MatDialogRef, MatDividerModule,
  MatExpansionModule,
  MatGridListModule, MatIconModule, MatInputModule, MatListModule, MatMenuModule,
  MatNativeDateModule, MatPaginatorModule, MatProgressBarModule, MatProgressSpinnerModule, MatRadioModule,
  MatRippleModule, MatSelectModule, MatSidenavModule, MatSliderModule, MatSlideToggleModule, MatSnackBarModule,
  MatSortModule,
  MatStepperModule, MatTableModule, MatTabsModule, MatToolbarModule, MatTooltipModule, MatTreeModule
} from '@angular/material';
import {HttpClientModule} from '@angular/common/http';
import {SingleTableDataComponent} from './single-table-data/single-table-data.component';
import {SearchComponent} from './utils/search/search.component';
import {AutoSizeInputModule} from 'ngx-autosize-input';
import {BsDatepickerModule} from 'ngx-bootstrap/datepicker';
import {CreateRowComponent} from './create-row/create-row.component';
import {VirtualScrollerModule} from 'ngx-virtual-scroller';
import {AuthDialogComponent} from './auth-dialog/auth-dialog.component';
import {MainPageComponent} from './main-page/main-page.component';
import {ToolbarComponent} from './toolbar/toolbar.component';
import {TopPanelComponent} from './top-panel/top-panel.component';
import {ContentComponent} from './content/content.component';
import {TableDataComponent} from './table-data/table-data.component';
import { SettingsContentComponent } from './settings-content/settings-content.component';
import { SettingsTableDataComponent } from './settings-table-data/settings-table-data.component';
import { FieldSettingsDialogComponent } from './field-settings-dialog/field-settings-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    TableDataComponent,
    SingleTableDataComponent,
    SearchComponent,
    CreateRowComponent,
    AuthDialogComponent,
    MainPageComponent,
    ToolbarComponent,
    TopPanelComponent,
    ContentComponent,
    SettingsContentComponent,
    SettingsTableDataComponent,
    FieldSettingsDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    MatNativeDateModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonToggleModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    HttpClientModule,
    MDBBootstrapModule.forRoot(),
    ReactiveFormsModule,
    AutoSizeInputModule,
    BsDatepickerModule.forRoot(),
    VirtualScrollerModule
  ],
  providers: [
    {provide: MatDialogRef, useValue: {}},
    {provide: MAT_DIALOG_DATA, useValue: []}
  ],
  bootstrap: [AppComponent],
  entryComponents: [CreateRowComponent, AuthDialogComponent, FieldSettingsDialogComponent]
})
export class AppModule {
}
