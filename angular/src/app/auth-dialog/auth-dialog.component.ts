import {Component, OnInit, ElementRef} from '@angular/core';
import {DataProviderService} from '../data-provider.service';
import {MatDialogRef} from '@angular/material';
import {NavigationExtras, Router} from '@angular/router';
import {CookieService} from '../cookie.service';

@Component({
  selector: 'app-auth-dialog',
  templateUrl: './auth-dialog.component.html',
  styleUrls: ['./auth-dialog.component.scss']
})
export class AuthDialogComponent implements OnInit {
  login: string;
  password: string;
  loginFailed = false;

  constructor(public dataProviderService: DataProviderService,
              public dialogRef: MatDialogRef<AuthDialogComponent>,
              public router: Router,
              private elRef: ElementRef) {
  }

  ngOnInit(): void {
    this.elRef.nativeElement.parentElement.style.boxShadow = 'none';
    this.login = CookieService.getCookie('TableEditingToolUsername');
    document.getElementById('password-input').onkeyup = (event) => {
      if (event.which === 13) {
        this.onLoginClick();
      }
    };
  }

  onLoginClick(): void {
    this.dataProviderService.authUser({login: this.login, password: this.password})
      .subscribe(result => {
        if (result.isAuthComplete) {
          CookieService.setCookie('TableEditingToolUsername', this.login);
          CookieService.setCookie('TableEditingToolToken', result.token, 24 * 60 * 60 * 1000);
          this.dialogRef.close();
        } else {
          this.loginFailed = true;
        }
      });
  }
}

