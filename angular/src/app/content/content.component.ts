import {Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild} from '@angular/core';
import {DataProviderService} from '../data-provider.service';
import {TableDataComponent} from '../table-data/table-data.component';
import {ToolbarComponent} from '../toolbar/toolbar.component';
import {SettingsContentComponent} from '../settings-content/settings-content.component';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit, OnChanges {

  @Output() tableSelected = new EventEmitter<string>();
  @Output() createNewTable = new EventEmitter<any>();
  @Output() tableListLoaded = new EventEmitter<any>();

  @Input() settingsMode;
  @Input() isFullMode;

  @ViewChild(TableDataComponent) editTable;
  @ViewChild(ToolbarComponent) toolbar;
  @ViewChild(SettingsContentComponent) settingsContent;

  public dictGroupsTableList = [];
  public tableNameList = [];
  public tableNamesMap = {};

  opened = true;
  selectedTableName: string;

  constructor(private dataProvider: DataProviderService) {
  }

  ngOnInit() {
    this.loadTableList();
    this.opened = !this.isFullMode;
  }

  loadTableList() {
    const tableListLoaded = this.dataProvider.getDictGroupsTableList();
    tableListLoaded.subscribe((result) => {
      this.dictGroupsTableList = result.dictGroupsTableList;
      this.tableNameList = result.tableNameList;
      this.tableNamesMap = result.tableNamesMap;
      this.tableListLoaded.emit();
    });
    return tableListLoaded;
  }

  ngOnChanges(changes) {
    Object.keys(changes).forEach((changedInput) => {
      this[changedInput] = changes[changedInput].currentValue;
    });
  }

  onTableClick(tableName) {
    this.dataProvider.checkTableAccessibility(tableName).subscribe((result) => {
      if (result.error) {
        alert('This table is not editable at the moment. ' + result.error);
      } else {
        if (!this.settingsMode) {
          this.toolbar.dataLoaded = false;
        }
        this.tableSelected.emit(tableName);
      }
    });
  }

  onNewTableClick() {
    if (!this.settingsMode) {
      this.toolbar.dataLoaded = false;
    }
    this.createNewTable.emit();
  }

  onConfigSaved(tableName) {
    this.loadTableList().subscribe(() => {
      setTimeout(() => {
        this.onTableClick(tableName);
      });
    });
  }

}
