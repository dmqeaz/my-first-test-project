import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {DataProviderService} from '../data-provider.service';
import * as moment from 'moment';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {CookieService} from "../cookie.service";

declare const $;

@Component({
  selector: 'app-create-row',
  templateUrl: './create-row.component.html',
  styleUrls: ['./create-row.component.css']
})
export class CreateRowComponent implements OnInit {

  tableName: string;
  fieldSettings = {};
  optionsForField = {};
  idField: string;
  fieldsToCreate = [];
  dictionaryIdField = {};
  dictionaryValueField = {};
  dictionaryFieldSettings = {};

  formGroup: FormGroup;
  submitted = false;

  constructor(private dialogRef: MatDialogRef<CreateRowComponent>,
              private dataProvider: DataProviderService,
              private formBuilder: FormBuilder,
              @Inject(MAT_DIALOG_DATA) private data) {
  }

  ngOnInit() {
    this.tableName = this.data.tableName;
    this.fieldSettings = this.data.fieldSettings;
    this.dictionaryFieldSettings = this.data.dictionaryFieldSettings;

    this.idField = this.data.idField;
    this.fieldsToCreate = Object.keys(this.fieldSettings).filter(field => field !== this.idField);

    this.dictionaryIdField = this.data.dictionaryIdField;
    this.dictionaryValueField = this.data.dictionaryValueField;

    Object.keys(this.fieldSettings).forEach(field => this.optionsForField[field] = []);

    const controls = {};
    this.fieldsToCreate.forEach((field) => {
      if (this.fieldSettings[field].required) {
        controls[field] = new FormControl('', [Validators.required]);
      } else {
        controls[field] = new FormControl('');
      }
    });

    this.formGroup = this.formBuilder.group(controls);

    this.loadDictionaryOptions();
  }

  get f() {
    return this.formGroup.controls;
  }

  loadDictionaryOptions() {
    Object.keys(this.fieldSettings).forEach((field) => {
      if (this.fieldSettings[field].isDictionaryValue) {
        this.dataProvider.getDictionaryOptions(this.tableName, this.getResultRow(), field).subscribe((options) => {
          this.optionsForField[field] = options;
        });
      }
    });
  }

  getDictionaryDisplayString(field, value) {
    const dictionaryName = this.fieldSettings[field].dictionaryName;
    const valueField = this.dictionaryValueField[dictionaryName];

    return this.formatValue(value, this.dictionaryFieldSettings[dictionaryName][valueField]);
  }

  formatValue(value, type) {
    if (value === '' || ((type === 'number' || type === 'date') && isNaN(value))) {
      return undefined;
    }
    switch (type) {
      case 'text':
        return '' + value;
      case 'number':
        return +value;
      case 'date':
        return moment(new Date(value)).format('DD.MM.YYYY');
      default:
        return value;
    }
  }

  onDateInputKeyPress(event) {
    const symbol = String.fromCharCode(event.which);
    return Boolean(symbol.match(/^[0-9\.\-\/]/));
  }

  onDatePickerValueChange(field, event) {
    const date = new Date(moment(event.srcElement.value, 'DD.MM.YYYY').toString());
    if (isNaN(+date)) {
      this.formGroup.controls[field].patchValue('');
    }
  }

  getResultRow() {
    const row = {};
    this.fieldsToCreate.forEach((field) => {
      row[field] = this.formatValue(this.f[field].value, this.fieldSettings[field].type);
    });
    return row;
  }

  onFieldBlur() {
    $(document.activeElement).blur();
  }

  onSaveClick() {
    $(document.activeElement).blur();
    this.submitted = true;
    if (this.formGroup.invalid) {
      return;
    }
    this.dataProvider.addNewRow(this.tableName, this.getResultRow(),
      CookieService.getCookie('TableEditingToolUsername')).subscribe(() => {
      this.dialogRef.close(true);
    });
  }

  onCancelClick() {
    this.dialogRef.close();
  }

}
