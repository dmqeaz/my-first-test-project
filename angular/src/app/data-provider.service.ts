import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {DataSettings} from './entities/data-settings';
import {catchError, map} from 'rxjs/internal/operators';
import {UpdateData} from "./entities/update-data";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class DataProviderService {

  private SERVER_URL = 'http://' + window.location.host;
  private getSettingsUrl = this.SERVER_URL + '/getSettings';
  private getDataUrl = this.SERVER_URL + '/getData';
  private getPreviewDataUrl = this.SERVER_URL + '/getPreviewData';
  private updateDataUrl = this.SERVER_URL + '/updateData';
  private getDictGroupsTableListUrl = this.SERVER_URL + '/getDictGroupsTableList';
  private checkTableAccessibilityUrl = this.SERVER_URL + '/checkTableAccessibility';
  private getDictionaryOptionsUrl = this.SERVER_URL + '/getDictionaryOptions';
  private getDictGroupsUrl = this.SERVER_URL + '/getDictGroups';
  private addNewRowUrl = this.SERVER_URL + '/addNewRow';
  private authUserUrl = this.SERVER_URL + '/authUser';
  private checkTokenUrl = this.SERVER_URL + '/checkToken';
  private logoutUserUrl = this.SERVER_URL + '/logoutUser';
  private loadTableConfigUrl = this.SERVER_URL + '/loadConfig';
  private saveTableConfigUrl = this.SERVER_URL + '/saveConfig';
  private checkTableConfigUrl = this.SERVER_URL + '/checkConfig';
  private loadDataFromConfigUrl = this.SERVER_URL + '/loadDataFromConfig';

  constructor(private http: HttpClient) {
  }

  getSettings(tableName): Observable<DataSettings> {
    return this.http
      .post<any>(this.getSettingsUrl, {tableName: tableName}, httpOptions)
      .pipe(catchError(this.handleError<any>('getSettings')));
  }

  getDictGroupsTableList(): Observable<any> {
    return this.http
      .post<any>(this.getDictGroupsTableListUrl, {}, httpOptions)
      .pipe(catchError(this.handleError<any>('getDictGroupsTableList')));
  }

  getData(tableName): Observable<any> {
    return this.http
      .post<any>(this.getDataUrl, {tableName: tableName}, httpOptions)
      .pipe(catchError(this.handleError<any>('getData')));
  }

  getPreviewData(tableName): Observable<any> {
    return this.http
      .post<any>(this.getPreviewDataUrl, {tableName: tableName}, httpOptions)
      .pipe(catchError(this.handleError<any>('getPreviewData')));
  }

  getDictionaryOptions(tableName, row, field): Observable<any> {
    return this.http
      .post<any>(this.getDictionaryOptionsUrl, {tableName: tableName, row: row, field: field}, httpOptions)
      .pipe(catchError(this.handleError<any>('getDictionaryOptions')));
  }

  getDictGroups(): Observable<any> {
    return this.http
      .post<any>(this.getDictGroupsUrl, {}, httpOptions)
      .pipe(catchError(this.handleError<any>('getDictGroups')));
  }

  updateData(data: UpdateData, login): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': login
      })
    };
    return this.http
      .post<any>(this.updateDataUrl, data, options)
      .pipe(catchError(this.handleError<any>('updateData')));
  }

  checkTableAccessibility(tableName): Observable<any> {
    return this.http
      .post<any>(this.checkTableAccessibilityUrl, {tableName: tableName}, httpOptions)
      .pipe(catchError(this.handleError<any>('checkTableAccessibility')));
  }

  addNewRow(tableName, row, login): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': login
      })
    };
    return this.http
      .post<any>(this.addNewRowUrl, {tableName: tableName, row: row}, options)
      .pipe(catchError(this.handleError<any>('addNewRow')));
  }

  authUser(user) {
    return this.http
      .post<any>(this.authUserUrl, user, httpOptions)
      .pipe(catchError(this.handleError<any>('authUser')));
  }

  checkToken(login, token) {
    return this.http
      .post<any>(this.checkTokenUrl, {login: login, token: token}, httpOptions)
      .pipe(catchError(this.handleError<any>('checkToken')));
  }

  logoutUser(login, token) {
    return this.http
      .post<any>(this.logoutUserUrl, {login: login, token: token}, httpOptions)
      .pipe(catchError(this.handleError<any>('logoutUser')));
  }

  loadTableConfig(tableName) {
    return this.http
      .post<any>(this.loadTableConfigUrl, {tableName: tableName}, httpOptions)
      .pipe(catchError(this.handleError<any>('loadTableConfig')));
  }

  saveTableConfig(tableName, prevFileName, tableConfig, prevDictGroupId) {
    return this.http
      .post<any>(this.saveTableConfigUrl, {
        tableName: tableName,
        prevFileName: prevFileName,
        tableConfig: tableConfig,
        prevDictGroupId: prevDictGroupId
      }, httpOptions)
      .pipe(catchError(this.handleError<any>('saveTableConfig')));
  }

  checkTableConfig(tableConfig, tableName) {
    return this.http
      .post<any>(this.checkTableConfigUrl, {tableConfig: tableConfig, tableName: tableName}, httpOptions)
      .pipe(catchError(this.handleError<any>('checkTableConfig')));
  }

  loadDataFromConfig(tableName, tableConfig) {
    return this.http
      .post<any>(this.loadDataFromConfigUrl, {tableName: tableName, tableConfig: tableConfig}, httpOptions)
      .pipe(catchError(this.handleError<any>('loadDataFromConfig')));
  }

  handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      alert((typeof error.error === 'string' ? error.error : 'Произошла ошибка'));
      throw error;
    };
  }
}
