export class DataSettings {
  tableNameList: string[];
  dictionaryNameList: string[];
  tableFieldList: {};
  dictionaryFieldList: {};
  tableFieldSettings: {};
  dictionaryIdField: {};
  dictionaryValueField: {};
  dictionaryFieldSettings: {};
  tableNamesMap: {};
  fieldNamesMap: {};
  tableIdField: {};
  dictionaryData: {};
  tableConfig: {};
}
