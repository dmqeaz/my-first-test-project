export class FieldSettings {
  isEditable: boolean;
  isDictionaryValue: boolean;
  dictionaryName: string;
}
