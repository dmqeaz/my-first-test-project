import {Component, Inject, Input, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-field-settings-dialog',
  templateUrl: './field-settings-dialog.component.html',
  styleUrls: ['./field-settings-dialog.component.scss', './ff-styles.scss', './ie-styles.scss']
})
export class FieldSettingsDialogComponent implements OnInit {

  fieldConfig: any = {};
  originFieldConfig = {};

  dataTypes = [
    {value: 'number', text: 'Число'},
    {value: 'date', text: 'Дата'},
    {value: 'text', text: 'Строка'}
  ];

  constructor(@Inject(MAT_DIALOG_DATA) public data,
              public dialogRef: MatDialogRef<FieldSettingsDialogComponent>) { }

  ngOnInit() {
    if(Object.keys(this.data.fieldConfig).length === 0) {
      this.data.fieldConfig.defaultColumnWidth = 200;
    }
    this.fieldConfig = this.data.fieldConfig;
    this.originFieldConfig = JSON.parse(JSON.stringify(this.fieldConfig));
  }

  onApplyClick() {
    this.dialogRef.close(true);
  }

  onCancelClick() {
    Object.keys(this.fieldConfig).forEach(key => delete this.fieldConfig[key]);
    Object.keys(this.originFieldConfig).forEach(key => this.fieldConfig[key] = this.originFieldConfig[key]);
    this.dialogRef.close(false);
  }

}
