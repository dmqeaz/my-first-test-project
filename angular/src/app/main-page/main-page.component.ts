import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {ContentComponent} from '../content/content.component';

import * as XLSX from 'xlsx';
import {TableDataComponent} from '../table-data/table-data.component';
import {SingleTableDataComponent} from '../single-table-data/single-table-data.component';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit, AfterViewInit {

  @ViewChild(ContentComponent) content;
  editTable: TableDataComponent;
  @Input() selectedTableName;
  @Input() isFullMode;

  settingsMode = false;

  constructor() {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.editTable = this.content.editTable;

    if (this.selectedTableName) {
      this.onTableSelected(this.selectedTableName);
    }
  }

  onTableSelected(tableName) {
    if (this.settingsMode === true) {
      this.selectedTableName = tableName;
      this.content.selectedTableName = tableName;
      this.content.settingsContent.tableName = tableName;
      this.content.settingsContent.tableNameDisplayed = this.content.tableNamesMap[tableName] || tableName;
      this.content.settingsContent.refreshConfig();
    } else {
      if (this.editTable.changeCurrentTable(tableName)) {
        this.content.toolbar.tableName = this.content.tableNamesMap[tableName] || tableName;
        this.selectedTableName = tableName;
        this.content.selectedTableName = tableName;
      }
    }
  }

  onCreateNewTable() {
    this.content.settingsContent.tableNameDisplayed = 'Новая таблица';
    const tableName = 'newTable.json';
    this.selectedTableName = tableName;
    this.content.selectedTableName = tableName;
    this.content.settingsContent.tableName = tableName;
    this.content.settingsContent.loadEmptyConfig();
  }

  onSettingsMode(settingsMode) {
    this.settingsMode = settingsMode;
    if (!this.settingsMode) {
      this.selectedTableName = this.content.tableNameList.find((n) => n === this.selectedTableName);
      this.content.toolbar.tableName = this.content.tableNamesMap[this.selectedTableName] || this.selectedTableName;
      if (!this.selectedTableName) {
        this.content.toolbar.tableName = 'Выберите таблицу для редактирования';
      }
      this.editTable.changeCurrentTable(this.selectedTableName);
    } else {
      if (this.selectedTableName) {
        setTimeout(() => {
          this.content.settingsContent.tableNameDisplayed = this.content.tableNamesMap[this.selectedTableName]
            || this.selectedTableName;
        }, 0);
      } else {
        setTimeout(() => {
          this.content.settingsContent.tableNameDisplayed = 'Выберите таблицу для редактирования';
        }, 0);
      }
      this.editTable.tableData = JSON.parse(JSON.stringify(this.editTable.originTableData));
    }
  }

  onTableListLoaded() {
    if (this.settingsMode === true) {
      this.content.settingsContent.tableNameDisplayed = this.content.tableNamesMap[this.selectedTableName]
        || this.selectedTableName
        || this.content.settingsContent.tableNameDisplayed;
    } else {
      this.content.toolbar.tableName = this.content.tableNamesMap[this.selectedTableName]
        || this.selectedTableName
        || this.content.toolbar.tableName;
    }
  }

  onExportExcelClick() {
    if (this.editTable.tableFieldList[this.selectedTableName]) {
      const wb = XLSX.utils.book_new();
      const data: any[][] = [
        this.editTable.tableFieldList[this.selectedTableName].map((field) => {
          return (this.editTable.fieldNamesMap[this.selectedTableName] || {})[field] || field;
        })
      ];

      const dictionaryValue = {};
      Object.keys(this.editTable.dictionaryData).forEach((dictionaryName) => {
        const singleDictionaryValue = {};
        this.editTable.dictionaryData[dictionaryName].forEach((row) => {
          singleDictionaryValue[row[this.editTable.dictionaryIdField[dictionaryName]]]
            = row[this.editTable.dictionaryValueField[dictionaryName]];
        });
        dictionaryValue[dictionaryName] = singleDictionaryValue;
      });

      this.editTable.tableComponent.searchComponent
        .getSortedData(this.editTable.tableData, this.editTable.tableComponent.sortParams)
        .forEach((row) => {
          const dataRow = [];
          this.editTable.tableFieldList[this.selectedTableName].forEach((field) => {
            if (this.editTable.tableFieldSettings[this.selectedTableName][field].isDictionaryValue) {
              const dictionaryName = this.editTable.tableFieldSettings[this.selectedTableName][field].dictionaryName;
              const dictionary = dictionaryValue[dictionaryName];
              dataRow.push(SingleTableDataComponent.formatValue(dictionary[row[field]],
                (this.editTable.dictionaryFieldSettings[dictionaryName][field] || {}).type));
            } else {
              dataRow.push(SingleTableDataComponent.formatValue(row[field],
                (this.editTable.tableFieldSettings[this.selectedTableName][field] || {}).type));
            }
          });
          data.push(dataRow);
        });

      XLSX.utils.book_append_sheet(wb, XLSX.utils.aoa_to_sheet(data), this.selectedTableName);
      XLSX.writeFile(wb, this.selectedTableName + '.xlsx');
    } else {
      alert("Пожалуйста, выберите таблицу");
    }
  }

}
