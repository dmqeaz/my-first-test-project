import {Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild} from '@angular/core';
import {DataProviderService} from '../data-provider.service';
import {SettingsTableDataComponent} from '../settings-table-data/settings-table-data.component';
import {MatExpansionPanel} from '@angular/material';

declare const $;

@Component({
  selector: 'app-settings-content',
  templateUrl: './settings-content.component.html',
  styleUrls: ['./settings-content.component.scss']
})
export class SettingsContentComponent implements OnInit, OnChanges {

  tableConfig: any = {};
  tableFieldList = [];
  originTableConfig: any = {};
  dictGroups = [];
  isNewTable;

  @ViewChild(SettingsTableDataComponent) tableDataComponent;
  @ViewChild('filterPanel') filterPanel: MatExpansionPanel;

  @Input() tableName;

  @Output() configSaved = new EventEmitter<any>();

  tableNameDisplayed;

  constructor(private dataProvider: DataProviderService) {
  }

  ngOnInit() {
    this.dataProvider.getDictGroups().subscribe((dictGroups) => {
      this.dictGroups = dictGroups;
    });
    this.tableConfig.dictGroupId = 0;
    if (this.tableName) {
      this.refreshConfig();
    }
  }

  ngOnChanges(changes) {
    if (changes.tableName && changes.tableName.currentValue !== this.tableName) {
      this.tableName = changes.tableName.currentValue;
      this.refreshConfig();
    }
  }

  refreshConfig() {
    this.dataProvider.loadTableConfig(this.tableName).subscribe((tableConfig) => {
      this.originTableConfig = tableConfig;
      this.tableConfig = JSON.parse(JSON.stringify(tableConfig));
      this.isNewTable = false;
      this.tableDataComponent.refreshData();
    });
  }

  loadEmptyConfig() {
    const config = {
      tableName: '',
      dbSchema: '',
      displayName: '',
      dictGroupId: 0,
      fields: [],
      isNewRowAllowed: false
    };
    this.originTableConfig = config;
    this.tableConfig = JSON.parse(JSON.stringify(config));
    this.isNewTable = true;
  }

  saveTableConfig() {
    this.dataProvider.checkTableConfig(this.tableConfig, this.isNewTable ? undefined : this.tableName)
      .subscribe((result) => {
        if (result.status === 'correct') {
          if (this.tableConfig.fields.find(f => f.name === this.tableConfig.idField) === undefined) {
            this.tableConfig.fields.push({name: this.tableConfig.idField, type: 'number'});
          }
          this.dataProvider.saveTableConfig(this.tableConfig.displayName + '.json',
            this.isNewTable ? undefined : this.tableName, this.tableConfig,
            this.isNewTable ? undefined : this.originTableConfig.dictGroupId)
            .subscribe(() => {
              this.configSaved.emit(this.tableConfig.displayName + '.json');
            });
        } else {
          alert('Не удалось сохранить конфигурацию. ' + result.error);
        }
      });
  }

  resetTableConfig() {
    this.tableConfig = JSON.parse(JSON.stringify(this.originTableConfig));
    if (!this.isNewTable) {
      this.tableDataComponent.refreshData();
    }
  }

  refreshData() {
    this.dataProvider.checkTableConfig(this.tableConfig, this.isNewTable ? undefined : this.tableName)
      .subscribe((result) => {
        if (result.status === 'correct') {
          this.tableDataComponent.loadDataFromConfig();
        } else {
          alert('Не удалось применить конфигурацию. ' + result.error);
        }
      });
  }

  addFilter() {
    this.tableConfig.filterRules = this.tableConfig.filterRules || [];
    this.tableConfig.filterRules.push({field: '', operator: '', value: ''});
    setTimeout(() => {
      this.filterPanel.open();
    });
  }

  deleteFilter(index) {
    this.tableConfig.filterRules.splice(index, 1);
    if (!this.tableConfig.filterRules.length) {
      this.filterPanel.close();
    }
  }

}
