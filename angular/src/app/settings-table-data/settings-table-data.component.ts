import {AfterViewInit, ChangeDetectorRef, Component, Input, OnChanges, OnInit} from '@angular/core';
import {SingleTableDataComponent} from '../single-table-data/single-table-data.component';
import {DataProviderService} from '../data-provider.service';
import {DataSettings} from '../entities/data-settings';
import {MatDialog, MatDialogConfig} from "@angular/material";
import {FieldSettingsDialogComponent} from "../field-settings-dialog/field-settings-dialog.component";

declare const $: any;

@Component({
  selector: 'app-settings-table-data',
  templateUrl: './settings-table-data.component.html',
  styleUrls: ['./settings-table-data.component.scss', './ie-styles.scss', './ff-styles.scss']
})
export class SettingsTableDataComponent implements OnInit, OnChanges {

  @Input() tableName;
  @Input() tableConfig;
  @Input() isNewTable;

  fieldsToDisplay = [];
  fieldNamesMap = {};
  tableDataToDisplay = [];
  fieldSettings = {};
  dictionaryDataById = {};
  dictionaryIdField;
  dictionaryValueField;
  dictionaryFieldSettings = {};
  dictionaryNameList = [];

  resizeTarget;
  resizeInProgress;
  resizeStartX;
  resizeStartWidth;

  dragInProgress;
  draggedColumnIndex;
  draggedColumn;
  dragOffset;
  dragStartX;

  constructor(private dataProvider: DataProviderService,
              public changeDetectorRef: ChangeDetectorRef,
              public dialog: MatDialog) {
  }

  static copyObject(from, to) {
    Object.keys(to).forEach(key => delete to[key]);
    Object.keys(from).forEach(key => to[key] = from[key]);
  }

  ngOnInit() {
    if (this.tableName && !this.isNewTable) {
      this.refreshData();
    }
  }

  ngOnChanges(changes) {
    if (changes.tableName && changes.tableName.currentValue) {
      this.tableName = changes.tableName.currentValue;
      if (!this.isNewTable) {
        this.refreshData();
      } else {
        this.tableDataToDisplay = [];
        this.fieldsToDisplay = [];
      }
    }
    if (changes.tableConfig) {
      this.tableConfig = changes.tableConfig.currentValue;
    }
  }

  refreshData() {
    this.loadSettings();
  }

  getTableWidth() {
    if (!this.tableConfig) {
      return 0;
    }
    let width = 0;
    this.fieldsToDisplay.forEach((field) => {
      width += (this.tableConfig.fields.find(f => f.name === field) || {}).defaultColumnWidth || 200;
    });
    return {'width': width + 'px'};
  }

  getColumnWidth(field) {
    const width = (this.tableConfig.fields.find(f => f.name === field) || {}).defaultColumnWidth || 200;
    return width;
  }

  isNumField(field) {
    if (this.fieldSettings[field].type === 'number') {
      return 1;
    }
    return 0;
  }

  onColumnResizeMouseDown(event, field) {
    event.stopPropagation();
    this.resizeTarget = event.target;
    this.resizeInProgress = true;
    this.resizeStartX = event.clientX;
    this.resizeStartWidth = $(this.resizeTarget).parent().width();
    this.initResizableColumns(field);
  }

  initResizableColumns(field) {
    document.onmousemove = (event) => {
      if (this.resizeInProgress) {
        const headerCell = $('.settings-table').find('.mat-column-' + field);
        const headerCellPadding = headerCell.css('padding-left').slice(0, -2);
        const deltaWidth = event.clientX - this.resizeStartX;
        const width = Math.max(this.resizeStartWidth + deltaWidth + 2 * headerCellPadding, 50);
        this.tableConfig.fields.find(f => f.name === field).defaultColumnWidth = width;
        headerCell.css({'width': width});
        const padding = headerCell.find('span').parent().css('padding-left').slice(0, -2);
        headerCell.find('span').css({'max-width': headerCell.find('span').parent().width() - 2 * padding});
        if (document.getSelection()) {
          document.getSelection().removeAllRanges();
        }
      }
    };
    document.onmouseup = () => {
      if (this.resizeInProgress) {
        this.resizeInProgress = false;
      }
    };
  }

  getDisplayString(row, field) {
    if (row[field] === null || row[field] === undefined) {
      return '';
    }
    return SingleTableDataComponent.formatValue(row[field], this.fieldSettings[field].type);
  }

  getDictionaryDisplayString(field, value) {
    const dictionaryName = this.fieldSettings[field].dictionaryName;
    const valueField = this.dictionaryValueField[dictionaryName];

    return SingleTableDataComponent.formatValue(value, this.dictionaryFieldSettings[dictionaryName][valueField].type);
  }

  getDictFieldType(field) {
    const dictionaryName = this.fieldSettings[field].dictionaryName;
    const valueField = this.dictionaryValueField[dictionaryName];
    const dictFieldType = this.dictionaryFieldSettings[dictionaryName][valueField].type;
    return dictFieldType;
  }

  loadSettings() {
    this.dataProvider.getSettings(this.tableName).subscribe((settings: DataSettings) => {
      this.fieldSettings = settings.tableFieldSettings[this.tableName];
      this.dictionaryIdField = settings.dictionaryIdField;
      this.dictionaryValueField = settings.dictionaryValueField;
      this.dictionaryFieldSettings = settings.dictionaryFieldSettings;

      this.fieldNamesMap = settings.fieldNamesMap[this.tableName];
      this.dictionaryNameList = settings.dictionaryNameList;

      this.dictionaryNameList.forEach((dictionaryName) => {
        this.dictionaryDataById[dictionaryName] = {};
        settings.dictionaryData[dictionaryName].forEach((row) => {
          this.dictionaryDataById[dictionaryName][row[settings.dictionaryIdField[dictionaryName]]] = row;
        });
      });

      this.fieldsToDisplay = settings.tableFieldList[this.tableName]
        .filter(field => field !== settings.tableIdField[this.tableName]);

      this.loadData();
    });
  }

  loadData() {
    this.dataProvider.getPreviewData(this.tableName).subscribe((data) => {
      this.tableDataToDisplay = data.tableData;
    });

    setTimeout(() => {
      this.fieldsToDisplay.forEach((field) => {
        const headerCell = $('.settings-table').find('.mat-column-' + field);
        headerCell.css('width', this.tableConfig.fields.find(f => f.name === field).defaultColumnWidth || 200);
        let padding = headerCell.find('span').parent().css('padding-left');
        padding = padding.slice(0, -2);
        headerCell.find('span').css({'max-width': headerCell.find('span').parent().width() - 2 * padding});
      });
    });
  }

  onHeaderMouseDown(event, field) {
    const filteredFields = this.tableConfig.fields.filter(f => f.name !== this.tableConfig.idField);
    this.dragInProgress = true;
    this.dragStartX = event.clientX;
    this.draggedColumnIndex = filteredFields.findIndex(f => f.name === field);
    this.draggedColumn = field;

    document.onmousemove = (e) => {
      if (!this.dragInProgress) {
        return;
      }
      if (document.getSelection()) {
        document.getSelection().removeAllRanges();
      }
      this.dragOffset = e.clientX - this.dragStartX;
      this.handleDrag(e, filteredFields);
    };
    document.onmouseup = () => {
      document.onmousemove = () => {
      };
      document.onmouseup = () => {
      };
      this.dragInProgress = false;
      this.draggedColumn = undefined;
      this.dragOffset = 0;
    };
  }

  handleDrag(e, filteredFields) {
    const direction = this.dragOffset > 0 ? 1 : -1;
    const nextColumnIndex = this.draggedColumnIndex + direction;
    while (nextColumnIndex >= 0 && nextColumnIndex < filteredFields.length
    && Math.abs(this.dragOffset) > (filteredFields[nextColumnIndex].defaultColumnWidth || 200)) {
      if (this.dragOffset > 0) {
        this.dragStartX += filteredFields[nextColumnIndex].defaultColumnWidth || 200;
      } else {
        this.dragStartX -= filteredFields[nextColumnIndex].defaultColumnWidth || 200;
      }
      this.dragOffset = e.clientX - this.dragStartX;

      let buf = filteredFields[nextColumnIndex];
      filteredFields[nextColumnIndex] = filteredFields[this.draggedColumnIndex];
      filteredFields[this.draggedColumnIndex] = buf;

      const i = this.tableConfig.fields
        .findIndex(f => f.name === buf.name);
      const j = this.tableConfig.fields
        .findIndex(f => f.name === filteredFields[nextColumnIndex].name);

      buf = this.tableConfig.fields[i];
      this.tableConfig.fields[i] = this.tableConfig.fields[j];
      this.tableConfig.fields[j] = buf;

      this.fieldsToDisplay = this.tableConfig.fields
        .map(f => f.name)
        .filter(f => f !== this.tableConfig.idField);

      this.draggedColumnIndex += direction;
    }
    if (nextColumnIndex === -1) {
      this.dragOffset = Math.max(0, this.dragOffset);
    }
    if (nextColumnIndex === filteredFields.length) {
      this.dragOffset = Math.min(0, this.dragOffset);
    }
  }

  loadDataFromConfig() {
    this.dataProvider.loadDataFromConfig(this.tableName, this.tableConfig).subscribe((settings) => {
      this.fieldSettings = settings.tableFieldSettings;
      this.fieldNamesMap = settings.fieldNamesMap;

      this.dictionaryNameList.forEach((dictionaryName) => {
        this.dictionaryDataById[dictionaryName] = {};
        settings.dictionaryData[dictionaryName].forEach((row) => {
          this.dictionaryDataById[dictionaryName][row[this.dictionaryIdField[dictionaryName]]] = row;
        });
      });

      this.fieldsToDisplay = settings.tableFieldList
        .filter(field => field !== settings.tableIdField);

      this.tableDataToDisplay = settings.tableData;

      setTimeout(() => {
        this.fieldsToDisplay.forEach((field) => {
          const headerCell = $('.settings-table').find('.mat-column-' + field);
          headerCell.css('width', this.tableConfig.fields.find(f => f.name === field).defaultColumnWidth || 200);
          let padding = headerCell.find('span').parent().css('padding-left');
          padding = padding.slice(0, -2);
          headerCell.find('span').css({'max-width': headerCell.find('span').parent().width() - 2 * padding});
        });
      });
    });
  }

  onEditColumnClick(field, event) {
    const config = new MatDialogConfig();

    config.width = '600px';
    config.disableClose = true;
    config.autoFocus = false;

    const originFieldConfig = JSON.parse(JSON.stringify(this.tableConfig));

    config.data = {
      fieldConfig: this.tableConfig.fields.find(f => f.name === field)
    };

    this.dialog.open(FieldSettingsDialogComponent, config).afterClosed().subscribe((update) => {
      if (update) {
        this.dataProvider.checkTableConfig(this.tableConfig, this.isNewTable ? undefined : this.tableName)
          .subscribe((result) => {
            if (result.status === 'correct') {
              this.loadDataFromConfig();
            } else {
              SettingsTableDataComponent.copyObject(originFieldConfig, this.tableConfig);
              alert('Не удалось применить конфигурацию. ' + result.error);
            }
          });
      }
    });
  }

  onAddColumnClick() {
    const config = new MatDialogConfig();
    const fieldConfig = {};

    config.width = '600px';
    config.disableClose = true;
    config.autoFocus = false;

    config.data = {
      fieldConfig: fieldConfig
    };

    this.dialog.open(FieldSettingsDialogComponent, config).afterClosed().subscribe((update) => {
      if (update) {
        const newConfig = JSON.parse(JSON.stringify(this.tableConfig));
        newConfig.fields.push(fieldConfig);
        this.dataProvider.checkTableConfig(newConfig, this.isNewTable ? undefined : this.tableName)
          .subscribe((result) => {
            if (result.status === 'correct') {
              SettingsTableDataComponent.copyObject(newConfig, this.tableConfig);
              this.loadDataFromConfig();
            } else {
              alert('Не удалось применить конфигурацию. ' + result.error);
            }
          });
      }
    });
  }

  onDeleteColumnClick(field, event) {
    this.tableConfig.fields = this.tableConfig.fields.filter(f => f.name !== field);
    this.loadDataFromConfig();
  }

}
