import {
  AfterViewInit, ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, OnInit,
  ViewChild
} from '@angular/core';
import {MatPaginator, MatSort} from '@angular/material';
import {SearchComponent} from '../utils/search/search.component';
import {fromEvent} from 'rxjs/index';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/internal/operators';
import * as moment from 'moment';
import {BsLocaleService, defineLocale} from 'ngx-bootstrap';
import {ruLocale} from 'ngx-bootstrap/locale';
import {DataProviderService} from '../data-provider.service';

defineLocale('ru', ruLocale);

declare const window: any;
declare const $: any;


@Component({
  selector: 'app-single-table-data',
  templateUrl: './single-table-data.component.html',
  styleUrls: ['./single-table-data.component.scss', './ie-styles.scss', './ff-styles.scss']
})
export class SingleTableDataComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy {

  @Input() tableData: Array<any>;
  @Input() fieldsToDisplay: Array<string>;
  @Input() fieldSettings: {};
  @Input() fieldNamesMap: {};
  @Input() idField: string;
  @Input() originTableData: Array<any>;
  @Input() dictionaryData: {};
  @Input() dictionaryDataById: {};
  @Input() dictionaryIdField: {};
  @Input() dictionaryValueField: {};
  @Input() dictionaryFieldSettings: {};
  @Input() selectedTableName: string;
  @Input() paginator: MatPaginator;
  @Input() loadingInProgress;
  @Input() searchComponent: SearchComponent;

  @ViewChild(MatSort) matSort;
  originTableDataById: {} = {};
  tableDataToDisplay = [];
  sortParams = {};
  filteredData = [];
  selectedField: string;

  hoveredRow = {};
  activeRow;
  activeRowCount = 0;

  resizeTarget: any;
  resizeInProgress = false;
  resizeStartX: number;
  resizeStartWidth: number;
  tableColumnWidth = {};
  columnWidth;
  tableWidth = 0;
  minTableWidth;

  datePickerModel = {};

  optionsForField: {} = {};
  rowWithLoadedOptions: {};

  currentDensity = 'middle';

  currentRow;
  currentField;

  paginatorSubscription;

  constructor(private localeService: BsLocaleService,
              private dataProvider: DataProviderService,
              public changeDetectorRef: ChangeDetectorRef) {
  }

  ngOnDestroy() {
    if (this.paginatorSubscription) {
      this.paginatorSubscription.unsubscribe();
    }
  }

  ngOnInit() {
    this.paginatorSubscription = this.paginator.page.subscribe(() => {
      localStorage.setItem(this.selectedTableName + '@paginatorSize', '' + this.paginator.pageSize);
      this.refreshDataToDisplay();
    });

    this.originTableData.forEach((row) => {
      this.originTableDataById[row[this.idField]] = row;
    });
    this.matSort.sortChange.subscribe((sortParams) => {
      this.sortParams = sortParams;
      this.searchComponent.filterData(sortParams);
      this.refreshData();
    });

    if (localStorage.getItem(this.selectedTableName + '@paginatorSize')) {
      this.paginator.pageSize = +localStorage.getItem(this.selectedTableName + '@paginatorSize');
    }

    this.refreshData();

    const searchObserver = fromEvent(document.body, 'SEARCH_INPUT_CHANGE').pipe(
      map(() => this.searchComponent.searchInput),
      debounceTime(300),
      distinctUntilChanged()
    );
    searchObserver.subscribe(() => {
      this.searchComponent.filterData(this.sortParams);
      this.paginator.length = this.filteredData.length;
      this.refreshDataToDisplay();
    });

    this.tableColumnWidth = JSON.parse(localStorage.getItem('tableColumnWidth') || '{}');
    this.columnWidth = this.tableColumnWidth[this.selectedTableName]
      || (this.tableColumnWidth[this.selectedTableName] = {});

    if (localStorage.getItem('tableDensity')) {
      this.currentDensity = localStorage.getItem('tableDensity');
    }
  }

  ngAfterViewInit() {
    let updateColumnWidth = true;
    this.fieldsToDisplay.forEach((field) => {
      if (!this.columnWidth[field]) {
        updateColumnWidth = false;
      }
    });
    this.minTableWidth = $('.resizable-data-table').parent().width();
    this.fieldsToDisplay.forEach((field) => {
      if (!updateColumnWidth) {
        this.columnWidth[field] = +this.fieldSettings[field].defaultColumnWidth
          || Math.max(this.minTableWidth / this.fieldsToDisplay.length, 50);
      }
      const headerCell = $('.mat-column-' + field);
      headerCell.css('width', this.columnWidth[field]);
      let padding = headerCell.find('span').parent().css('padding-left');
      padding = padding.slice(0, -2);
      headerCell.find('span').css({'max-width': headerCell.find('span').parent().width() - 2 * padding});
      this.tableWidth += this.columnWidth[field];
    });
    localStorage.setItem('tableColumnWidth', JSON.stringify(this.tableColumnWidth));
    this.localeService.use('ru');

    window.onTableScroll = this.onTableScroll.bind(this);

    document.onmousemove = () => {
    };
  }

  ngOnChanges(changes) {
    Object.keys(changes).forEach((changedInput) => {
      this[changedInput] = changes[changedInput].currentValue;
    });
    this.originTableData.forEach((row) => {
      this.originTableDataById[row[this.idField]] = row;
    });
    if (!this.loadingInProgress) {
      this.refreshData();
    }
  }

  refreshData() {
    this.searchComponent.data = this.tableData;
    this.filteredData = this.searchComponent.filteredData = [];
    this.searchComponent.filterData(this.sortParams);
    setTimeout(() => {
      this.paginator.length = this.filteredData.length;
    });
    this.refreshDataToDisplay();
  }

  refreshDataToDisplay() {
    let pageIndex = this.paginator.pageIndex;
    const pageSize = this.paginator.pageSize;
    this.paginator.pageIndex = pageIndex = Math.min(pageIndex, Math.ceil(this.filteredData.length / pageSize) - 1);
    this.tableDataToDisplay = this.filteredData.slice(pageIndex * pageSize,
      Math.min(this.filteredData.length, (pageIndex + 1) * pageSize));
  }

  cellValueChanged(row, field) {
    const originRow = this.originTableDataById[row[this.idField]];
    if (this.fieldSettings[field].type === 'date') {
      return new Date(row[field]).toLocaleDateString() !== new Date(originRow[field]).toLocaleDateString();
    }
    return !(originRow && (originRow[field] == row[field]));
  }

  getCellStyle(row, field) {
    const style = this.cellValueChanged(row, field) ? {
      'color': 'blue'
    } : {};
    style['width'] = this.columnWidth[field] - 22 + 'px';
    return style;
  }

  loadDictionaryOptions(row) {
    if (row === this.rowWithLoadedOptions || row === undefined) {
      return;
    }
    this.rowWithLoadedOptions = row;
    this.fieldsToDisplay.forEach((field) => {
      if (this.fieldSettings[field].isDictionaryValue && this.fieldSettings[field].isEditable) {
        const dictionaryName = this.fieldSettings[field].dictionaryName;
        const idField = this.dictionaryIdField[dictionaryName];

        this.dataProvider.getDictionaryOptions(this.selectedTableName, row, field).subscribe((options) => {
          if (this.checkActiveRow(row)) {
            this.optionsForField[field] = options;
          }
          options.forEach((option) => {
            if (!this.dictionaryDataById[dictionaryName][option[idField]]) {
              this.dictionaryDataById[dictionaryName][option[idField]] = option;
              this.dictionaryData[dictionaryName].push(option);
            }
          });
        });
      }
    });
  }

  setDefaultDictionaryOptions(row) {
    this.rowWithLoadedOptions = undefined;
    this.fieldsToDisplay.forEach((field) => {
      if (this.fieldSettings[field].isDictionaryValue) {
        this.optionsForField[field] = [this.dictionaryDataById[this.fieldSettings[field].dictionaryName][row[field]] || {}];
      }
    });
  }

  mouseEnterRow(row) {
    const isActive = this.checkActiveRow(row);
    this.hoveredRow = row;
    if (!isActive && this.checkActiveRow(row)) {
      this.updateDatePickerModel(row);
      this.setDefaultDictionaryOptions(row);
      setTimeout(() => {
        if (this.checkActiveRow(row)) {
          this.loadDictionaryOptions(row);
        }
      }, 100);
    }
  }

  mouseLeaveRow(row) {
    if (this.hoveredRow === row) {
      this.hoveredRow = undefined;
    }
  }

  onColumnResizeMouseDown(event, field) {
    this.resizeTarget = event.target;
    this.resizeInProgress = true;
    this.resizeStartX = event.clientX;
    this.resizeStartWidth = $(this.resizeTarget).parent().width();
    this.initResizableColumns(field);
  }

  initResizableColumns(field) {
    document.onmousemove = (event) => {
      if (this.resizeInProgress) {
        const headerCell = $('.mat-column-' + field);
        const headerCellPadding = headerCell.css('padding-left').slice(0, -2);
        const deltaWidth = event.clientX - this.resizeStartX;
        const width = Math.max(this.resizeStartWidth + deltaWidth + 2 * headerCellPadding, 50);
        this.tableWidth = this.tableWidth - this.columnWidth[field] + width;
        this.columnWidth[field] = width;
        headerCell.css({'width': width});
        const padding = headerCell.find('span').parent().css('padding-left').slice(0, -2);
        headerCell.find('span').css({'max-width': headerCell.find('span').parent().width() - 2 * padding});
        if (document.getSelection()) {
          document.getSelection().removeAllRanges();
        }
      }
    };
    document.onmouseup = () => {
      if (this.resizeInProgress) {
        this.resizeInProgress = false;
        localStorage.setItem('tableColumnWidth', JSON.stringify(this.tableColumnWidth));
      }
    };
  }

  getTableWidth() {
    return {'width': this.tableWidth + 'px'};
  }

  onRowFocus(row) {
    if (this.activeRow !== row) {
      this.activeRow = row;
      this.activeRowCount = 1;
      this.updateDatePickerModel(row);
      this.loadDictionaryOptions(this.hoveredRow);
    } else {
      this.activeRowCount++;
    }
  }

  onRowBlur(row) {
    if (this.activeRow === row) {
      this.activeRowCount--;
      if (this.activeRowCount < 1) {
        this.activeRow = undefined;
        this.updateDataFromModel(row);
        if (this.hoveredRow !== undefined) {
          this.updateDatePickerModel(this.hoveredRow);
          this.loadDictionaryOptions(this.hoveredRow);
        }
      }
    }
  }

  onTableScroll() {
    while (this.activeRow !== undefined) {
      this.onRowBlur(this.activeRow);
    }
  }

  onDateFieldInput(row, field, event) {
    const date = new Date(moment(event.srcElement.value, 'DD.MM.YYYY').toString());
    if (!isNaN(+date)) {
      this.datePickerModel[field].setTime(date.getTime());
    } else {
      this.datePickerModel[field].setTime(new Date(row[field]).getTime());
    }
  }

  onDatePickerSelect(datepicker) {
    if (!datepicker.isOpen) {
      $(datepicker._datepicker._elementRef.nativeElement).click();
    }
  }

  onDatePickerHidden(row, datepicker) {
    if (datepicker._datepicker._elementRef.nativeElement !== document.activeElement) {
      this.onRowBlur(row);
    }
  }

  onDatePickerBlur(row, datepicker) {
    if (!datepicker.isOpen) {
      this.onRowBlur(row);
    }
  }

  onDateInputKeyPress(event) {
    const symbol = String.fromCharCode(event.which);
    return Boolean(symbol.match(/^[0-9\.\-\/]/));
  }

  checkActiveRow(row) {
    return (row === this.hoveredRow && this.activeRow === undefined) || (row === this.activeRow);
  }

  getDisplayString(row, field) {
    if (row[field] === null || row[field] === undefined) {
      return '';
    }
    return SingleTableDataComponent.formatValue(row[field], this.fieldSettings[field].type);
  }

  getDictionaryDisplayString(field, value) {
    const dictionaryName = this.fieldSettings[field].dictionaryName;
    const valueField = this.dictionaryValueField[dictionaryName];

    return SingleTableDataComponent.formatValue(value, this.dictionaryFieldSettings[dictionaryName][valueField]);
  }

  static formatValue(value, type) {
    switch (type) {
      case 'text':
        return '' + value;
      case 'number':
        return +value;
      case 'date':
        return moment(new Date(value)).format('DD.MM.YYYY');
      default:
        return value;
    }
  }

  updateDatePickerModel(row) {
    this.fieldsToDisplay.forEach((field) => {
      if (this.fieldSettings[field].type === 'date') {
        this.datePickerModel[field] = new Date(row[field]);
      }
    });
  }

  updateDataFromModel(row) {
    this.fieldsToDisplay.forEach((field) => {
      if (this.fieldSettings[field].type === 'date') {
        if (this.datePickerModel[field]) {
          this.datePickerModel[field].setHours(0, 0, 0, 0);
          this.datePickerModel[field].setTime(this.datePickerModel[field].getTime()
            - this.datePickerModel[field].getTimezoneOffset() * 60000);
          row[field] = this.datePickerModel[field].toISOString();
        }
      }
    });
  }

  getInputType(field) {
    if (['date', 'number', 'text'].indexOf(this.fieldSettings[field].type) !== -1) {
      return this.fieldSettings[field].type;
    }
    return 'text';
  }

  getDictFieldType(field) {
    const dictionaryName = this.fieldSettings[field].dictionaryName;
    const valueField = this.dictionaryValueField[dictionaryName];
    const dictFieldType = this.dictionaryFieldSettings[dictionaryName][valueField].type;
    return dictFieldType;
  }

  static checkForIE() {
    return navigator.appName === 'Microsoft Internet Explorer'
      || !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv:11/))
      || (typeof $.browser !== 'undefined' && +$.browser.msie === 1);
  }

  onDropDownOpenedChanged(opened, row, field, select) {
    if (opened) {
      $(select.overlayDir._overlayRef._backdropElement).css({
        'pointer-events': 'none',
        'background': 'none'
      });
    }
  }

  onSelectOptionMouseDown(row, field, value) {
    this.currentRow = row;
    this.currentField = field;

    if (value === row[field]) {
      this.currentRow = this.currentField = undefined;
    }
  }

  onSelectRowBlur(row, field, select) {
    if (this.currentRow !== row || this.currentField !== field) {
      select.close();
      this.onRowBlur(row);
    } else {
      this.currentRow = this.currentField = undefined;
    }
  }

  onSelectValueChange(row, select) {
    setTimeout(() => {
      $(select._elementRef.nativeElement).blur();
    });
    this.rowWithLoadedOptions = undefined;
    this.loadDictionaryOptions(row);
    this.onRowBlur(row);
  }
}
