import {Component, EventEmitter, Inject, OnInit, Output, ViewChild} from '@angular/core';
import {DataProviderService} from '../data-provider.service';
import {DataSettings} from '../entities/data-settings';
import {MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatPaginator} from '@angular/material';
import * as XLSX from 'xlsx';
import {SingleTableDataComponent} from '../single-table-data/single-table-data.component';
import {CreateRowComponent} from '../create-row/create-row.component';
import {CookieService} from '../cookie.service';
import {SearchComponent} from '../utils/search/search.component';

declare let $: any;

@Component({
  selector: 'app-table-data',
  templateUrl: './table-data.component.html',
  styleUrls: ['./table-data.component.scss', './ff-styles.scss']
})
export class TableDataComponent implements OnInit {

  tableNameList: string[] = [];
  dictionaryNameList: string[] = [];
  tableFieldList: {} = {};
  dictionaryFieldList: {} = {};
  tableFieldSettings: {} = {};
  dictionaryIdField: {} = {};
  dictionaryValueField: {} = {};
  dictionaryFieldSettings: {} = {};
  tableNamesMap: {} = {};
  fieldNamesMap: {} = {};
  tableIdField: {} = {};
  tableConfig: {} = {};
  tableFieldsToDisplay = [];
  paginator: MatPaginator;
  searchComponent: SearchComponent;
  @ViewChild(SingleTableDataComponent) tableComponent;

  tableData = [];
  dictionaryData: {} = {};
  dictionaryDataById: {} = {};

  originTableData = [];
  originDictionaryData: {} = {};
  originTableDataById = {};

  settingsForSearch = [];
  settingsLoaded = false;
  selectedTableName: string;

  isNewRowAllowed = false;

  isIE = SingleTableDataComponent.checkForIE();

  loadingInProgress = false;
  changeTableInProgress = false;

  @Output() allowNewRow = new EventEmitter<boolean>();
  @Output() dataLoaded = new EventEmitter<any>();
  @Output() settingsLoadedOutput = new EventEmitter<any>();

  constructor(private dataProvider: DataProviderService,
              private dialog: MatDialog,
              @Inject(MAT_DIALOG_DATA) private dialogData: any) {
  }

  ngOnInit() {
    this.selectedTableName = this.dialogData.selectedTableName;
    if (!this.settingsLoaded && this.selectedTableName !== undefined) {
      this.loadSettings();
    }

    this.initTableView();
  }

  initTableView() {
    const dialogElement = $('.main-dialog-title').parent().parent();
    dialogElement.resizable({
      alsoResize: '.mat-dialog-content',
      stop: () => {
        if ($('.main-dialog-content').length) {
          const dialogSettings = {
            dialogWidth: dialogElement.outerWidth(),
            dialogHeight: dialogElement.outerHeight()
          };
          localStorage.setItem(this.selectedTableName + '@dialogSettings', JSON.stringify(dialogSettings));
        }
      }
    });

    dialogElement.css('overflow', 'hidden');
    dialogElement.css('max-width', '95vw');
    dialogElement.css('min-width', '40vw');
    dialogElement.css('max-height', '95vh');
    dialogElement.css('min-height', '50vh');
    dialogElement.parent().css('max-width', '95vw');
    document.body.style.overflow = 'hidden';

    let dialogSettings = JSON.parse(localStorage.getItem(this.selectedTableName + '@dialogSettings'));
    dialogSettings = dialogSettings || {
      dialogWidth: 1000,
      dialogHeight: 700
    };

    dialogElement.css('width', dialogSettings.dialogWidth + 'px');
    dialogElement.css('height', dialogSettings.dialogHeight + 'px');
  }

  loadSettings() {
    this.loadingInProgress = true;
    this.dataProvider.getSettings(this.selectedTableName).subscribe((settings: DataSettings) => {
      this.tableNameList = settings.tableNameList;
      this.dictionaryNameList = settings.dictionaryNameList;

      this.tableFieldList = settings.tableFieldList;
      this.dictionaryFieldList = settings.dictionaryFieldList;

      this.tableFieldSettings = settings.tableFieldSettings;
      this.dictionaryData = settings.dictionaryData;
      this.dictionaryIdField = settings.dictionaryIdField;
      this.dictionaryValueField = settings.dictionaryValueField;
      this.dictionaryFieldSettings = settings.dictionaryFieldSettings;

      this.tableNamesMap = settings.tableNamesMap;
      this.fieldNamesMap = settings.fieldNamesMap;

      this.tableIdField = settings.tableIdField;
      this.tableConfig = settings.tableConfig;

      this.dictionaryNameList.forEach((dictionaryName) => {
        this.dictionaryDataById[dictionaryName] = {};
        this.dictionaryData[dictionaryName].forEach((row) => {
          this.dictionaryDataById[dictionaryName][row[this.dictionaryIdField[dictionaryName]]] = row;
        });
      });

      this.tableFieldsToDisplay = this.tableFieldList[this.selectedTableName]
        .filter(field => field !== this.tableIdField[this.selectedTableName])

      this.loadData();

      this.isNewRowAllowed = this.tableConfig[this.selectedTableName].isNewRowAllowed;
      this.allowNewRow.emit(this.isNewRowAllowed);

      this.settingsLoaded = true;

      this.settingsForSearch['idField'] = this.tableIdField[this.selectedTableName];
      this.settingsForSearch['fieldList'] = this.tableFieldsToDisplay;
      this.settingsForSearch['fieldSettings']  = this.tableFieldSettings[this.selectedTableName];
      this.settingsForSearch['dictionaryData'] = this.dictionaryData;
      this.settingsForSearch['dictionaryIdField'] = this.dictionaryIdField;
      this.settingsForSearch['dictionaryValueField'] = this.dictionaryValueField;
      this.settingsForSearch['dictionaryFieldSettings'] = this.dictionaryFieldSettings;
      this.settingsLoadedOutput.emit(this.settingsForSearch);
    });
  }

  loadData() {
    this.loadingInProgress = true;
    this.dataProvider.getData(this.selectedTableName).subscribe((data) => {

      this.tableData = data.tableData;

      this.originTableData = JSON.parse(JSON.stringify(this.tableData));
      this.originDictionaryData = JSON.parse(JSON.stringify(this.dictionaryData));

      this.originTableDataById = {};
      this.originTableData.forEach((row) => {
        this.originTableDataById[row[this.tableIdField[this.selectedTableName]]] = row;
      });

      this.loadingInProgress = false;
      this.changeTableInProgress = false;
      this.dataLoaded.emit();
    });
  }

  changeCurrentTable(tableName) {
    this.changeTableInProgress = true;
    let valueChanged = false;
    if (this.selectedTableName) {
      this.tableData.forEach((row) => {
        this.tableFieldList[this.selectedTableName].forEach((field) => {
          const originRow = this.originTableDataById[row[this.tableIdField[this.selectedTableName]]];
          valueChanged = valueChanged || !(originRow && (originRow[field] == row[field]));
        });
      });
    }
    if (!valueChanged || confirm('Несохраненные данные будут утеряны. Продолжить?')) {
      this.selectedTableName = tableName;
      if (this.selectedTableName) {
        this.loadSettings();
        this.initTableView();
      }
      return true;
    }
    this.changeTableInProgress = false;
    this.dataLoaded.emit();
    return false;
  }

  onPaginatorInited(paginator) {
    this.paginator = paginator;
  }

  onSearchInited(searchComponent) {
    this.searchComponent = searchComponent;
  }

  onTableDensityChanged(density) {
    localStorage.setItem('tableDensity', density);
    this.tableComponent.currentDensity = density;
  }

  onSaveClick() {
    if (JSON.stringify(this.tableData) + JSON.stringify(this.dictionaryData)
      === JSON.stringify(this.originTableData) + JSON.stringify(this.originDictionaryData)) {
      return;
    }

    const filteredData = [];
    for (let i = 0; i < this.tableData.length; i++) {
      const rowId = this.tableData[i][this.tableIdField[this.selectedTableName]];
      const originRow = this.originTableDataById[rowId];
      if (JSON.stringify(this.tableData[i]) !== JSON.stringify(originRow)) {
        const changes = {};
        Object.keys(this.tableData[i]).forEach((field) => {
          if (field === this.tableIdField[this.selectedTableName]
            || JSON.stringify(this.tableData[i][field]) !== JSON.stringify(originRow[field])) {
            changes[field] = this.tableData[i][field];
          }
        });
        filteredData.push(changes);
      }
    }
    const tableChanges = filteredData.length ? filteredData : undefined;

    this.dataProvider.updateData({tableName: this.selectedTableName, tableChanges: tableChanges},
      CookieService.getCookie('TableEditingToolUsername'))
      .subscribe(() => {
        this.loadData();
      });
  }

  onCancelClick() {
    this.tableData = JSON.parse(JSON.stringify(this.originTableData));
    this.dictionaryData = JSON.parse(JSON.stringify(this.originDictionaryData));
  }

  onNewRowClick() {
    const config = new MatDialogConfig();

    config.width = '25%';
    config.disableClose = true;
    config.autoFocus = false;

    config.data = {
      tableName: this.selectedTableName,
      fieldSettings: this.tableFieldSettings[this.selectedTableName],
      idField: this.tableIdField[this.selectedTableName],
      dictionaryIdField: this.dictionaryIdField,
      dictionaryValueField: this.dictionaryValueField,
      dictionaryFieldSettings: this.dictionaryFieldSettings
    };

    this.dialog.open(CreateRowComponent, config).afterClosed().subscribe(update => {
      const buttonList = document.getElementsByClassName('mat-raised-button');
      const buttonArray = [].slice.call(buttonList);
      buttonArray.forEach((currentButton) => {
        currentButton.classList.remove('cdk-program-focused');
        currentButton.classList.add('cdk-mouse-focused');
      });

      if (update) {
        this.loadData();
      }
    });
  }

  getTableStyle() {
    if (!this.isIE) {
      return {};
    }
    return {'height': ($('app-single-table-data').parent().height() - 60) + 'px'};
  }
}
