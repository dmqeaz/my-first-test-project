import {AfterViewInit, ViewChild, Component, EventEmitter, OnInit, Output} from '@angular/core';
import {MatPaginator} from '@angular/material';
import {SearchComponent} from '../utils/search/search.component';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit, AfterViewInit {

  @Output() tableDensityChanged = new EventEmitter<any>();
  @Output() paginatorInited = new EventEmitter<MatPaginator>();
  @Output() searchInited = new EventEmitter<any>();

  @Output() addNewRow = new EventEmitter<any>();
  @Output() saveData = new EventEmitter<any>();
  @Output() resetData = new EventEmitter<any>();

  @ViewChild(MatPaginator) paginator;
  @ViewChild(SearchComponent) searchComponent;

  tableName = 'Выберите таблицу для редактирования';
  densityModel;
  settings;
  isNewRowAllowed = false;
  dataLoaded = false;

  constructor() {
  }

  ngOnInit() {
    this.densityModel = localStorage.getItem('tableDensity') || 'middle';
  }

  ngAfterViewInit() {
    this.paginatorInited.emit(this.paginator);
  }

  onInitSettings(settings) {
    this.settings = settings;
    setTimeout(() => {
      this.searchInited.emit(this.searchComponent);
    }, 0);
  }

}
