import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DataProviderService} from '../data-provider.service';
import {CookieService} from '../cookie.service';

@Component({
  selector: 'app-top-panel',
  templateUrl: './top-panel.component.html',
  styleUrls: ['./top-panel.component.scss']
})

export class TopPanelComponent implements OnInit, AfterViewInit {

  @Output() exportExcel = new EventEmitter<any>();
  @Output() settingsMode = new EventEmitter<boolean>();
  @Output() saveData = new EventEmitter<any>();
  @Output() resetData = new EventEmitter<any>();
  @Output() refreshData = new EventEmitter<any>();
  @Output() addFilter = new EventEmitter<any>();
  @Output() addColumn = new EventEmitter<any>();

  @Input() blocked;

  isSettingsMode = false;
  dataLoaded = false;

  constructor(private dataProvider: DataProviderService) {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
  }

  onLogoutClick() {
    const login = CookieService.getCookie('TableEditingToolUsername');
    const token = CookieService.getCookie('TableEditingToolToken');
    this.dataProvider.logoutUser(login, token).subscribe(() => {
      window.location.assign('');
    });
  }

}
