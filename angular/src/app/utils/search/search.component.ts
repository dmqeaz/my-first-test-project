import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-search',
  styleUrls: ['./search.component.css'],
  templateUrl: './search.component.html'
})
export class SearchComponent {

  public searchInput = '';
  public data = [];
  public filteredData = [];
  @Input() fieldSettings: {};
  @Input() dictionaryData: {};
  @Input() dictionaryIdField: {};
  @Input() dictionaryValueField: {};
  @Input() dictionaryFieldSettings: {};
  @Input() fieldList: Array<any>;
  @Input() idField: string;

  constructor() {
  }

  onInputChange() {
    document.body.dispatchEvent(new Event('SEARCH_INPUT_CHANGE', {}));
  }

  compare(a, b, field, sortOrder) {
    let type = this.fieldSettings[field].type;
    if (this.fieldSettings[field].isDictionaryValue) {
      const dictionaryName = this.fieldSettings[field].dictionaryName;
      const idField = this.dictionaryIdField[dictionaryName];
      const valueField = this.dictionaryValueField[dictionaryName];
      a = this.dictionaryData[dictionaryName].find(dictRow => dictRow[idField] === a[field]) || {};
      b = this.dictionaryData[dictionaryName].find(dictRow => dictRow[idField] === b[field]) || {};
      type = this.dictionaryFieldSettings[dictionaryName][valueField].type;
      field = valueField;
    }
    a = a[field];
    b = b[field];
    if (!a || !b) {
      return sortOrder === 'asc' ? a ? -1 : 1 : a ? 1 : -1;
    }
    switch (type) {
      case 'text':
        a = ('' + a).trim().toUpperCase();
        b = ('' + b).trim().toUpperCase();
        break;
      case 'number':
        a = +a;
        b = +b;
        break;
      case 'date':
        a = +new Date(a);
        b = +new Date(b);
        break;
      default:
        break;
    }
    return sortOrder === 'asc' ? a < b ? -1 : 1 : a > b ? -1 : 1;
  }

  filterData(sortParams): void {
    if (!this.fieldList || !this.fieldList.length) {
      while (this.filteredData.length) {
        this.filteredData.pop();
      }
      this.data.forEach(row => this.filteredData.push(row));
      return;
    }
    sortParams = sortParams || {};
    const searchString = this.searchInput.trim().toUpperCase();
    const sortField = sortParams.direction ? sortParams.active : this.idField;
    const sortOrder = sortParams.direction || 'asc';

    const extendedData = this.data.map((row) => {
      let rowString = '';
      this.fieldList.forEach((field) => {
        if (!this.fieldSettings[field].isDictionaryValue) {
          if (this.fieldSettings[field].type !== 'date') {
            rowString += ('' + row[field]).toUpperCase();
          } else {
            rowString += new Date(row[field]).toLocaleDateString();
          }
        } else {
          const idField = this.dictionaryIdField[this.fieldSettings[field].dictionaryName];
          const valueField = this.dictionaryValueField[this.fieldSettings[field].dictionaryName];
          let val = this.dictionaryData[this.fieldSettings[field].dictionaryName].find(dictRow => dictRow[idField] === row[field]);
          val = val || {};
          val = val[valueField] || '';
          rowString += ('' + val).toUpperCase();
        }
        rowString += '#';
      });
      let value = searchString ? rowString.indexOf(searchString) : 0;
      if (value === -1) {
        value = 100000;
        const searchArray = searchString.split(' ');
        searchArray.forEach((word) => {
          const position = rowString.indexOf(word);
          value -= position !== -1 ? word.length : 0;
        });
      }
      return {
        row: row,
        value: value
      };
    });

    while (this.filteredData.length > 0) {
      this.filteredData.pop();
    }

    extendedData.filter((a) => {
      return a.value < 100000;
    }).sort((a, b) => {
      return this.compare(a.row, b.row, sortField, sortOrder);
    }).map((extendedRow) => {
      return extendedRow.row;
    }).forEach((row) => {
      this.filteredData.push(row);
    });
  }

  getSortedData(data, sortParams) {
    const sortField = sortParams.direction ? sortParams.active : this.idField;
    const sortOrder = sortParams.direction || 'asc';

    return data.sort((a, b) => this.compare(a, b, sortField, sortOrder));
  }
}

