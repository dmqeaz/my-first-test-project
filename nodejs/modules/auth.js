const jsonParser = (require('body-parser')).json();
const ldap = require('ldapjs');

global.tokenStorage = {};

const xorString = (string, keyString) => {
    let code = 0;
    for (let i = 0; i < keyString.length; i++) {
        code = (code + keyString.charCodeAt(i) * (i + 1)) % 100;
    }

    let result = '';
    for (let i = 0; i < string.length; i++) {
        result += String.fromCharCode((string.charCodeAt(i) ^ code) % 26 + 'a'.charCodeAt(0));
    }

    return result;
};

const generateNewToken = (login) => {
    const token = xorString(new Date().toISOString(), login);
    tokenStorage[login] = {
        token: token,
        validUntil: +new Date() + 24 * 60 * 60 * 1000
    };

    return token;
};

app.post('/authUser', jsonParser, (req, res) => {
    let login = req.body.login;
    let password = req.body.password;

    if (!password) {
        res.json({isAuthComplete: false}).end();
        return;
    }

    const client = ldap.createClient({
        url: LDAP_URL
    });

    client.bind(login + '@' + AD_DOMAIN, password, function (err) {
        if (err) {
            client.unbind();
            logger.warn(err);
            res.json({isAuthComplete: false}).end();
        } else {
            client.unbind();
            res.json({isAuthComplete: true, token: generateNewToken(login)}).end();
        }
    });
});

app.post('/checkToken', jsonParser, (req, res) => {
    let login = req.body.login;
    let token = req.body.token;

    const storedToken = tokenStorage[login] || {};

    if (storedToken.token === token && storedToken.validUntil > +new Date()) {
        storedToken.validUntil = +new Date() + 24 * 60 * 60 * 1000;
        res.json({isTokenValid: true}).end();
    } else {
        res.json({isTokenValid: false}).end();
    }
});

app.post('/logoutUser', jsonParser, (req, res) => {
    const login = req.body.login;
    const token = req.body.token;

    if (tokenStorage[login] && tokenStorage[login].token === token) {
        tokenStorage[login] = undefined;
    }

    res.end();
});