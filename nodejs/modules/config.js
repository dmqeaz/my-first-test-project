const jsonParser = (require('body-parser')).json();
const fs = require('fs');
const _ = require('underscore');

app.post('/loadConfig', jsonParser, (req, res) => {
    const fileName = req.body.tableName;
    try {
        const config = JSON.parse(fs.readFileSync('./settings/table_config/' + fileName, 'utf-8'));
        res.json(config).end();
    } catch (e) {
        logger.error(e);
        res.status(500).end();
    }
});

app.post('/saveConfig', jsonParser, (req, res) => {
    const fileName = req.body.tableName;
    const prevFileName = req.body.prevFileName;
    const config = req.body.tableConfig;
    const prevDictGroupId = req.body.prevDictGroupId;
    try {
        if (typeof prevFileName !== 'undefined' && fileName !== prevFileName) {
            fs.unlinkSync('./settings/table_config/' + prevFileName);
        }
        fs.writeFileSync('./settings/table_config/' + fileName, JSON.stringify(config));
        global.reloadConfig(fileName, prevFileName, prevDictGroupId);
        res.end();
    } catch (e) {
        logger.error(e);
        res.status(500).end();
    }
});

app.post('/checkConfig', jsonParser, (req, res) => {
    try {
        const config = req.body.tableConfig;
        const tableName = req.body.tableName;
        if (_.uniq(config.fields.map(f => f.name)).length !== config.fields.length) {
            res.json({error: 'Config has duplicate fields'}).end();
            return;
        }

        if (tableName) {
            if (tableNameList.find((t) => t !== tableName && tableNamesMap[t] === config.displayName)) {
                res.json({error: 'Table display name must be unique'}).end();
                return;
            }
        } else {
            if (tableNameList.find((t) => tableNamesMap[t] === config.displayName)) {
                res.json({error: 'Table display name must be unique'}).end();
                return;
            }
        }

        if (config.filterRules) {
            if (config.filterRules.find((rule) => {
                    return rule.field === '' || rule.operator === '' || rule.value === '';
                })) {
                res.json({error: 'Filter has empty fields'}).end();
                return;
            }
            if (config.filterRules.find((rule) => {
                    return config.fields.map(f => f.name).indexOf(rule.field) === -1;
                })) {
                res.json({error: 'Filter field not found in table config'}).end();
                return;
            }
        }

        if (!config.idField) {
            res.json({error: 'ID field not set'}).end();
            return;
        }

        db.checkTableConfig(config).then(() => {
            res.json({status: 'correct'}).end();
        }).catch((err) => {
            res.json({error: err}).end();
        });
    } catch (err) {
        logger.error(err);
        if (!global.jdbcInited) {
            res.status(500).json("Can't connect to db").end();
        } else {
            res.status(500).json("Unexpected error occurred. Maybe config is incorrect").end();
        }
    }
});

app.post('/loadDataFromConfig', jsonParser, (req, res) => {
    try {
        const config = req.body.tableConfig;

        const tableFieldSettings = {};
        const fieldNamesMap = {};
        const tableFieldList = [];

        config.fields.forEach((field) => {
            tableFieldSettings[field.name] = field;
            if (field.isDictionaryValue) {
                tableFieldSettings[field.name].dictionaryName = tableFieldSettings[field.name].dictionaryDBSchema
                    + '.' + tableFieldSettings[field.name].dictionaryName;
            }
            fieldNamesMap[field.name] = field.displayName || field.name;
            tableFieldList.push(field.name);
        });

        const tableName = config.dbSchema + '.' + config.tableName;
        const fields = config.fields.map(f => f.name).join(', ');
        const fieldType = {};

        config.fields.forEach((field) => {
            fieldType[field.name] = field.type;
        });

        const argType = {};
        const args = [];
        let sql = 'SELECT ' + fields + ' FROM ' + tableName + ' WHERE 1 = 1 ';

        (config.filterRules || []).forEach((rule) => {
            sql += 'AND ' + rule.field + rule.operator + '? ';
            argType[args.length] = fieldType[rule.field];
            args.push(rule.value);
        });

        sql += ' ORDER BY (SELECT NULL) OFFSET 0 ROWS FETCH NEXT 10 ROWS ONLY';

        db.execute(sql, {fieldType: fieldType, args: args, argType: argType}).then((result) => {
            const dictionaryData = {};
            const usedIds = {};
            const dictionaryPromiseList = [];

            dictionaryNameList.forEach((dictionaryName) => {
                dictionaryData[dictionaryName] = [];
            });

            config.fields.forEach((field) => {
                if (field.isDictionaryValue) {
                    const dictionaryName = field.dictionaryName;
                    const dictionaryIdField = global.dictionaryIdField[dictionaryName];
                    const fields = dictionaryFieldList[dictionaryName].join(', ');
                    const fieldType = {};

                    dictionaryFieldList[dictionaryName].forEach((dictionaryField) => {
                        fieldType[dictionaryField] = dictionaryFieldSettings[dictionaryName][dictionaryField].type;
                    });

                    const promise = new Promise((resolve, reject) => {
                        db.execute('SELECT ' + fields + ' FROM ' + dictionaryName + ' WHERE ' + dictionaryIdField +
                            ' IN(SELECT ' + field.name + ' FROM ' + tableName + ')', {fieldType: fieldType})
                            .then((result) => {
                                usedIds[dictionaryName] = usedIds[dictionaryName] || {};
                                result.forEach((row) => {
                                    if (!usedIds[dictionaryName][row[dictionaryIdField]]) {
                                        dictionaryData[dictionaryName].push(row);
                                        usedIds[dictionaryName][row[dictionaryIdField]] = true;
                                    }
                                });
                                resolve();
                            }).catch(reject);
                    });

                    dictionaryPromiseList.push(promise);
                }
            });

            Promise.all(dictionaryPromiseList).then(() => {
                res.json({
                    tableFieldSettings: tableFieldSettings,
                    fieldNamesMap: fieldNamesMap,
                    tableFieldList: tableFieldList,
                    tableIdField: config.idField,
                    tableData: result,
                    dictionaryData: dictionaryData
                }).end();
            }).catch((err) => {
                res.status(500).json(err).end();
            });
        }).catch((err) => {
            res.status(500).json(err).end();
        });
    } catch (err) {
        logger.error(err);
        if (!global.jdbcInited) {
            res.status(500).json("Can't connect to db").end();
        } else {
            res.status(500).json("Unexpected error occurred. Maybe config is incorrect").end();
        }
    }
});

app.post('/getPreviewData', jsonParser, (req, res) => {
    try {
        const fileName = req.body.tableName;
        const tableName = fileToTableNamesMap[fileName];
        const fields = tableFieldList[fileName].join(', ');
        const fieldType = {};

        tableFieldList[fileName].forEach((field) => {
            fieldType[field] = tableFieldSettings[fileName][field].type;
        });

        const argType = {};
        const args = [];
        let sql = 'SELECT ' + fields + ' FROM ' + tableName + ' WHERE 1 = 1 ';

        tableFilterRules[fileName].forEach((rule) => {
            sql += 'AND ' + rule.field + ' ' + rule.operator;
            if (rule.value === null) {
                sql += ' null ';
            } else {
                sql += ' ? ';
                argType[args.length] = fieldType[rule.field];
                args.push(rule.value);
            }
        });

        sql += ' ORDER BY (SELECT NULL) OFFSET 0 ROWS FETCH NEXT 10 ROWS ONLY';

        db.execute(sql, {fieldType: fieldType, args: args, argType: argType}).then((result) => {
            res.json({tableData: result}).end();
        }).catch((err) => {
            res.status(500).json(err).end();
        });
    } catch (err) {
        logger.error(err);
        if (!global.jdbcInited) {
            res.status(500).json("Can't connect to db").end();
        } else {
            res.status(500).json("Unexpected error occurred. Maybe config is incorrect").end();
        }
    }
});