const java = require('java');
const _ = require('underscore');
const moment = require('moment');

module.exports = {
    jdbcDataTypeCode: {
        text: [-4, -3, -1, 1, 12, 1111],
        number: [-7, -6, -5, -2, 0, 2, 3, 4, 5, 6, 7, 8],
        date: [91, 92, 93]
    },

    execute(query, options) {
        options = options || {};
        const args = options.args || [];
        const argType = options.argType || {};
        const fieldType = options.fieldType || {};

        let isUpdateQuery = false;
        isUpdateQuery = query.toUpperCase().indexOf('INSERT') !== -1
            || query.toUpperCase().indexOf('UPDATE') !== -1;

        return new Promise((resolve, reject) => {
            try {
                db.jdbc.reserve((err, connObj) => {
                    if (err) {
                        logger.error(err);
                        reject();
                        return;
                    }

                    const conn = connObj.conn;
                    conn.prepareStatement(query, (err, preparedStatement) => {
                        if (err) {
                            logger.error(err);
                            reject();
                            return;
                        }
                        db.jdbc.release(connObj, (err) => {
                            if (err) {
                                logger.error(err);
                                reject();
                                return;
                            }

                            const promises = [];
                            args.forEach((arg, ind) => {
                                promises.push(new Promise((resolve, reject) => {
                                    const setterFunction = {number: 'setDouble', text: 'setString', date: 'setDate'};
                                    const funcName = setterFunction[argType[ind] || 'string'];

                                    const callback = (err) => {
                                        if (err) {
                                            logger.error(err);
                                            reject();
                                        } else {
                                            resolve();
                                        }
                                    };

                                    if (argType[ind] !== 'date') {
                                        preparedStatement[funcName](ind + 1, this.format(arg, argType[ind]), callback)
                                    } else {
                                        preparedStatement[funcName](ind + 1, this.format(arg, 'javaDate'), null, callback)
                                    }
                                }));
                            });

                            Promise.all(promises).then(() => {
                                if (!isUpdateQuery) {
                                    preparedStatement.executeQuery((err, resultSet) => {
                                        if (err) {
                                            logger.error(err);
                                            reject();
                                            preparedStatement.close(() => {
                                            });
                                            return;
                                        }
                                        resultSet.toObjArray((err, result) => {
                                            if (err) {
                                                logger.error(err);
                                                reject();
                                                preparedStatement.close(() => {
                                                });
                                                resultSet.close(() => {
                                                });
                                            } else {
                                                result.forEach((row) => {
                                                    Object.keys(row).forEach((key) => {
                                                        if (typeof row[key] === 'object' && row[key]) {
                                                            row[key] = JSON.parse(row[key]);
                                                        }
                                                        row[key] = this.format(row[key], fieldType[key]);
                                                    });
                                                });
                                                resolve(result);
                                                preparedStatement.close(() => {
                                                });
                                                resultSet.close(() => {
                                                });
                                            }
                                        });
                                    });
                                } else {
                                    preparedStatement.executeUpdate((err) => {
                                        if (err) {
                                            logger.error(err);
                                            reject();
                                        } else {
                                            resolve();
                                        }
                                        preparedStatement.close(() => {
                                        });
                                    });
                                }
                            });
                        });
                    });
                });
            } catch(err) {
                logger.error(err);
                reject("Can't connect to db");
            }
        });
    },

    checkTableAccessibility(fileName) {
        return new Promise((resolve, reject) => {
            const tableName = fileToTableNamesMap[fileName];
            const tablesToCheck = [tableName];
            const fieldsToCheck = {};
            let schemasToCheck = [];
            let rejected = false;

            if (!tableFieldSettings[fileName]) {
                reject('Table ' + tableName + ' config file not found');
                return;
            }

            fieldsToCheck[tableName] = Object.keys(tableFieldSettings[fileName]);

            Object.values(tableFieldSettings[fileName]).forEach((field) => {
                if (field.isDictionaryValue) {
                    const dictionaryName = field.dictionaryName;

                    if (!dictionaryFieldSettings[dictionaryName]) {
                        rejected = true;
                        reject('Dictionary ' + dictionaryName + ' not found');
                        return;
                    }

                    tablesToCheck.push(dictionaryName);
                    fieldsToCheck[dictionaryName] = Object.keys(dictionaryFieldSettings[dictionaryName]);
                }
            });

            schemasToCheck = _.uniq(tablesToCheck.map(tableName => tableName.slice(0, tableName.indexOf('.'))));

            if (rejected) {
                return;
            }

            try {
                db.jdbc.reserve((err, connObj) => {
                    if (err) {
                        logger.error(err);
                        reject('Failed to connect');
                        return;
                    }

                    const conn = connObj.conn;

                    conn.getMetaData((err, meta) => {
                        if (err) {
                            logger.error(err);
                            reject('Failed to get db metadata');
                            return;
                        }

                        db.jdbc.release(connObj, (err) => {
                            if (err) {
                                logger.error(err);
                                reject();
                                return;
                            }

                            const checkAllSchemasPromises = [];

                            schemasToCheck.forEach((schema) => {
                                checkAllSchemasPromises.push(new Promise((resolve, reject) => {
                                    meta.getTables(null, schema, '%', ['TABLE', 'VIEW'], (err, res) => {
                                        if (err) {
                                            logger.error(err);
                                            reject('Failed to get table list');
                                            return;
                                        }
                                        res.toObjArray((err, tableArray) => {
                                            if (err) {
                                                logger.error(err);
                                                reject('Failed to get table list from result set');
                                                return;
                                            }

                                            const tableExist = {};

                                            tableArray.forEach((table) => {
                                                Object.keys(table).forEach((key) => {
                                                    table[key.toLowerCase()] = table[key];
                                                });
                                            });

                                            tableArray.map(table => table.table_schem + '.' + table.table_name)
                                                .forEach(tableName => tableExist[tableName] = true);

                                            const promises = [];

                                            tablesToCheck.filter((tableName) => {
                                                return tableName.slice(0, tableName.indexOf('.')) === schema;
                                            }).forEach((tableName) => {
                                                if (!tableExist[tableName]) {
                                                    rejected = true;
                                                    reject('Table ' + tableName + ' not found in db');
                                                    return;
                                                }

                                                const checkTableColumnsPromise = new Promise((succeed, failed) => {
                                                    const name = tableName.slice(tableName.indexOf('.') + 1, tableName.length);

                                                    meta.getColumns(null, schema, name, '%', (err, columnsResultSet) => {
                                                        if (err) {
                                                            logger.error(err);
                                                            failed('Failed to get column list ' + tableName);
                                                            return;
                                                        }
                                                        columnsResultSet.toObjArray((err, columns) => {
                                                            if (err) {
                                                                logger.error(err);
                                                                failed('Failed to get column list from result set' + tableName);
                                                                return;
                                                            }

                                                            for (let i = 0; i < columns.length; i++) {
                                                                if (tableFieldSettings[fileName][columns[i].COLUMN_NAME]) {
                                                                    const type = tableFieldSettings[fileName][columns[i].COLUMN_NAME].type;
                                                                    if (this.jdbcDataTypeCode[type].indexOf(+columns[i].DATA_TYPE) === -1) {
                                                                        failed('In table ' + tableName + ' column "'
                                                                            + columns[i].COLUMN_NAME + '" has incompatible type');
                                                                        return;
                                                                    }
                                                                }
                                                            }

                                                            columns = columns.map(columnObj => columnObj.COLUMN_NAME);

                                                            const notExistingColumn = fieldsToCheck[tableName]
                                                                .find(field => columns.indexOf(field) === -1);

                                                            if (notExistingColumn !== undefined) {
                                                                failed('Column "' + notExistingColumn + '" not found in table ' + tableName);
                                                            } else {
                                                                succeed();
                                                            }
                                                        });
                                                    });
                                                });

                                                promises.push(checkTableColumnsPromise);
                                            });

                                            if (rejected) {
                                                return;
                                            }

                                            Promise.all(promises).then(resolve).catch(reject);
                                        });
                                    });
                                }));
                            });

                            Promise.all(checkAllSchemasPromises).then(resolve).catch(reject);
                        });
                    });
                });
            } catch(err) {
                logger.warn(err);
                reject("Can't connect to db");
            }
        });
    },

    checkTableConfig(config) {
        return new Promise((resolve, reject) => {
            const tableName = config.dbSchema + '.' + config.tableName;
            const tablesToCheck = [tableName];
            const fieldsToCheck = {};
            let schemasToCheck = [];
            let rejected = false;

            const tableFieldSettings = {};
            config.fields.forEach(f => tableFieldSettings[f.name] = f);

            fieldsToCheck[tableName] = Object.keys(tableFieldSettings);

            config.fields.forEach((field) => {
                if (field.isDictionaryValue) {
                    const dictionaryName = field.dictionaryDBSchema + '.' + field.dictionaryName;

                    if (!dictionaryFieldSettings[dictionaryName]) {
                        rejected = true;
                        reject('Dictionary ' + dictionaryName + ' not found');
                        return;
                    }

                    tablesToCheck.push(dictionaryName);
                    fieldsToCheck[dictionaryName] = Object.keys(dictionaryFieldSettings[dictionaryName]);
                }
            });

            schemasToCheck = _.uniq(tablesToCheck.map(tableName => tableName.slice(0, tableName.indexOf('.'))));

            if (rejected) {
                return;
            }

            try {
                db.jdbc.reserve((err, connObj) => {
                    if (err) {
                        logger.error(err);
                        reject('Failed to connect');
                        return;
                    }

                    const conn = connObj.conn;

                    conn.getMetaData((err, meta) => {
                        if (err) {
                            logger.error(err);
                            reject('Failed to get db metadata');
                            return;
                        }

                        db.jdbc.release(connObj, (err) => {
                            if (err) {
                                logger.error(err);
                                reject();
                                return;
                            }

                            const checkAllSchemasPromises = [];

                            schemasToCheck.forEach((schema) => {
                                checkAllSchemasPromises.push(new Promise((resolve, reject) => {
                                    meta.getTables(null, schema, '%', ['TABLE', 'VIEW'], (err, res) => {
                                        if (err) {
                                            logger.error(err);
                                            reject('Failed to get table list');
                                            return;
                                        }
                                        res.toObjArray((err, tableArray) => {
                                            if (err) {
                                                logger.error(err);
                                                reject('Failed to get table list from result set');
                                                return;
                                            }

                                            const tableExist = {};

                                            tableArray.forEach((table) => {
                                                Object.keys(table).forEach((key) => {
                                                    table[key.toLowerCase()] = table[key];
                                                });
                                            });

                                            tableArray.map(table => table.table_schem + '.' + table.table_name)
                                                .forEach(tableName => tableExist[tableName] = true);

                                            const promises = [];

                                            tablesToCheck.filter((tableName) => {
                                                return tableName.slice(0, tableName.indexOf('.')) === schema;
                                            }).forEach((tableName) => {
                                                if (!tableExist[tableName]) {
                                                    rejected = true;
                                                    reject('Table ' + tableName + ' not found in db');
                                                    return;
                                                }

                                                const checkTableColumnsPromise = new Promise((succeed, failed) => {
                                                    const name = tableName.slice(tableName.indexOf('.') + 1, tableName.length);

                                                    meta.getColumns(null, schema, name, '%', (err, columnsResultSet) => {
                                                        if (err) {
                                                            logger.error(err);
                                                            failed('Failed to get column list ' + tableName);
                                                            return;
                                                        }
                                                        columnsResultSet.toObjArray((err, columns) => {
                                                            if (err) {
                                                                logger.error(err);
                                                                failed('Failed to get column list from result set' + tableName);
                                                                return;
                                                            }

                                                            for (let i = 0; i < columns.length; i++) {
                                                                if (tableFieldSettings[columns[i].COLUMN_NAME]) {
                                                                    const type = tableFieldSettings[columns[i].COLUMN_NAME].type;
                                                                    if (this.jdbcDataTypeCode[type].indexOf(+columns[i].DATA_TYPE) === -1) {
                                                                        failed('In table ' + tableName + ' column "'
                                                                            + columns[i].COLUMN_NAME + '" has incompatible type');
                                                                        return;
                                                                    }
                                                                }
                                                            }

                                                            columns = columns.map(columnObj => columnObj.COLUMN_NAME);

                                                            const notExistingColumn = fieldsToCheck[tableName]
                                                                .find(field => columns.indexOf(field) === -1);

                                                            if (notExistingColumn !== undefined) {
                                                                failed('Column "' + notExistingColumn + '" not found in table ' + tableName);
                                                            } else {
                                                                succeed();
                                                            }
                                                        });
                                                    });
                                                });

                                                promises.push(checkTableColumnsPromise);
                                            });

                                            if (rejected) {
                                                return;
                                            }

                                            Promise.all(promises).then(resolve).catch(reject);
                                        });
                                    });
                                }));
                            });

                            Promise.all(checkAllSchemasPromises).then(resolve).catch(reject);
                        });
                    });
                });
            } catch(err) {
                logger.warn(err);
                reject("Can't connect to db");
            }
        });
    },

    checkDictionarySettings() {
        const schemasToCheck = _.uniq(dictionaryNameList.map((dictionary) => {
            return dictionary.slice(0, dictionary.indexOf('.'));
        }));

        const failedDictionaries = [];

        try {
            db.jdbc.reserve((err, connObj) => {
                if (err) {
                    logger.error(err);
                    return;
                }

                const conn = connObj.conn;

                conn.getMetaData((err, meta) => {
                    if (err) {
                        logger.error(err);
                        return;
                    }

                    db.jdbc.release(connObj, (err) => {
                        if (err) {
                            logger.error(err);
                            return;
                        }

                        const checkAllSchemasPromises = [];

                        schemasToCheck.forEach((schema) => {
                            checkAllSchemasPromises.push(new Promise((resolve, reject) => {
                                meta.getTables(null, schema, '%', ['TABLE', 'VIEW'], (err, res) => {
                                    if (err) {
                                        logger.error(err);
                                        reject();
                                        return;
                                    }
                                    res.toObjArray((err, tableArray) => {
                                        if (err) {
                                            logger.error(err);
                                            reject();
                                            return;
                                        }

                                        const tableExist = {};

                                        tableArray.forEach((table) => {
                                            Object.keys(table).forEach((key) => {
                                                table[key.toLowerCase()] = table[key];
                                            });
                                        });

                                        tableArray.map(table => table.table_schem + '.' + table.table_name)
                                            .forEach(tableName => tableExist[tableName] = true);

                                        const promises = [];

                                        dictionaryNameList.filter((dictionaryName) => {
                                            return dictionaryName.slice(0, dictionaryName.indexOf('.')) === schema;
                                        }).forEach((dictionaryName) => {
                                            if (!tableExist[dictionaryName]) {
                                                failedDictionaries.push(dictionaryName);
                                                logger.warn('Dictionary ' + dictionaryName + ' not found in db');
                                                return;
                                            }

                                            const checkTableColumnsPromise = new Promise((succeed, failed) => {
                                                const name = dictionaryName.slice(dictionaryName.indexOf('.') + 1, dictionaryName.length);

                                                meta.getColumns(null, schema, name, '%', (err, columnsResultSet) => {
                                                    if (err) {
                                                        logger.error(err);
                                                        failedDictionaries.push(dictionaryName);
                                                        succeed();
                                                        return;
                                                    }
                                                    columnsResultSet.toObjArray((err, columns) => {
                                                        if (err) {
                                                            logger.error(err);
                                                            failedDictionaries.push(dictionaryName);
                                                            succeed();
                                                            return;
                                                        }

                                                        for (let i = 0; i < columns.length; i++) {
                                                            if (dictionaryFieldSettings[dictionaryName][columns[i].COLUMN_NAME]) {
                                                                const type = dictionaryFieldSettings[dictionaryName][columns[i].COLUMN_NAME].type;
                                                                if (this.jdbcDataTypeCode[type].indexOf(+columns[i].DATA_TYPE) === -1) {
                                                                    failed('In table ' + dictionaryName + ' column "'
                                                                        + columns[i].COLUMN_NAME + '" has incompatible type');
                                                                    return;
                                                                }
                                                            }
                                                        }

                                                        columns = columns.map(columnObj => columnObj.COLUMN_NAME || columnObj.column_name);

                                                        const notExistingColumn = dictionaryFieldList[dictionaryName]
                                                            .find(field => columns.indexOf(field) === -1);

                                                        if (notExistingColumn !== undefined) {
                                                            logger.warn('Column "' + notExistingColumn + '" not found in table ' + dictionaryName);
                                                            failedDictionaries.push(dictionaryName);
                                                        }
                                                        succeed();
                                                    });
                                                });
                                            });

                                            promises.push(checkTableColumnsPromise);
                                        });

                                        Promise.all(promises).then(resolve).catch(reject);
                                    });
                                });
                            }));
                        });

                        Promise.all(checkAllSchemasPromises).then(() => {
                            if (failedDictionaries.length === 0) {
                                logger.info(`Dictionary settings checked`);
                            } else {
                                logger.error('Errors in ' + failedDictionaries.length + ' dictionary config files');
                                logger.error('Failed dictionaries: ' + failedDictionaries.join(', '));
                                dictionaryNameList = dictionaryNameList.filter((dictionaryName) => {
                                    return failedDictionaries.indexOf(dictionaryName) === -1
                                });
                            }
                        }).catch(logger.error);
                    });
                });
            });
        } catch(err) {
            logger.error(err);
        }
    },

    format(value, type) {
        switch (type) {
            case 'text':
                return '' + value;
            case 'number':
                return +value;
            case 'date':
                return new Date(value);
            case 'javaDate':
                let jsDate = moment(value, 'DD.MM.YYYY').toDate();
                jsDate = isNaN(jsDate) ? new Date(value) : jsDate;
                return java.newInstanceSync('java.sql.Date', java.newInstanceSync('java.lang.Long', '' + +jsDate));
            default:
                return value;
        }
    }
};