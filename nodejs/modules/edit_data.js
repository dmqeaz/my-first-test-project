const jsonParser = (require('body-parser')).json();

app.post('/getData', jsonParser, (req, res) => {
    try {
        const fileName = req.body.tableName;
        const tableName = fileToTableNamesMap[fileName];
        const fields = tableFieldList[fileName].join(', ');
        const fieldType = {};

        tableFieldList[fileName].forEach((field) => {
            fieldType[field] = tableFieldSettings[fileName][field].type;
        });

        const argType = {};
        const args = [];
        let sql = 'SELECT ' + fields + ' FROM ' + tableName + ' WHERE 1 = 1 ';

        tableFilterRules[fileName].forEach((rule) => {
            sql += 'AND ' + rule.field + ' ' + rule.operator;
            if(rule.value === null) {
                sql += ' null ';
            } else {
                sql += ' ? ';
                argType[args.length] = fieldType[rule.field];
                args.push(rule.value);
            }
        });


        db.execute(sql, {fieldType: fieldType, args: args, argType: argType}).then((result) => {
            res.json({tableData: result}).end();
        }).catch((err) => {
            res.status(500).json(err).end();
        });
    } catch (err) {
        logger.error(err);
        if (!global.jdbcInited) {
            res.status(500).json("Can't connect to db").end();
        } else {
            res.status(500).json("Unexpected error occurred. Maybe config is incorrect").end();
        }
    }
});

app.post('/updateData', jsonParser, (req, res) => {
    try {
        const tableChanges = req.body.tableChanges;
        const fileName = req.body.tableName;
        const tableName = fileToTableNamesMap[fileName];

        const idField = tableIdField[fileName];

        if (!idField) {
            logger.error('Editable row must have ID field');
            res.status(500).end();
            return;
        }

        const updatePromises = [];

        for (let i = 0; i < tableChanges.length; i++) {
            const changedFields = tableFieldList[fileName]
                .filter(field => tableChanges[i][field] !== undefined);

            if (changedFields.filter(field => tableChanges[i][field] !== null).length === 0) {
                continue;
            }


            const values = changedFields
                .filter(field => tableChanges[i][field] !== null && field !== idField)
                .map(field => tableChanges[i][field]);
            const valueType = {};

            changedFields
                .filter(field => tableChanges[i][field] !== null && field !== idField)
                .forEach((field, ind) => {
                    valueType[ind] = tableFieldSettings[fileName][field].type;
                });

            const updateFields = changedFields
                .filter(field => tableChanges[i][field] !== null && field !== idField)
                .map((field) => {
                    return field + '=' + '?';
                }).join(', ');

            if (!values.length) {
                continue;
            }

            values.push(tableChanges[i][idField]);
            valueType[values.length - 1] = tableFieldSettings[fileName][idField].type;

            updatePromises.push(new Promise((resolve, reject) => {
                db.execute('UPDATE ' + tableName + ' SET ' + updateFields + ' WHERE ' + idField + '= ?', {
                    args: values,
                    argType: valueType
                }).then(() => {
                    let textMessage = req.get('Authorization') + ' updated ' + tableName + '.' +
                        ' Row id = ' + tableChanges[i][idField] + '. Changed fields: ';
                    changedFields
                        .filter(f => f !== idField)
                        .forEach(field => textMessage += field + ' = ' + tableChanges[i][field] + ', ');
                    logger.info(textMessage.slice(0, -2));
                    resolve();
                }).catch(reject);
            }));
        }

        Promise.all(updatePromises)
            .then(() => {
                res.end()
            })
            .catch((err) => {
                res.status(500).json(err).end();
            })
    } catch (err) {
        logger.error(err);
        if (!global.jdbcInited) {
            res.status(500).json("Can't connect to db").end();
        } else {
            res.status(500).json("Unexpected error occurred. Maybe config is incorrect").end();
        }
    }
});

app.post('/addNewRow', jsonParser, (req, res) => {
    try {
        const fileName = req.body.tableName;
        const tableName = fileToTableNamesMap[fileName];
        const row = req.body.row;
        const args = [];
        const argType = {};

        tableFieldList[fileName].forEach((field) => {
            if (row[field] !== undefined) {
                argType[args.length] = tableFieldSettings[fileName][field].type;
                args.push(row[field]);
            }
        });

        const fields = tableFieldList[fileName]
            .filter(field => row[field] !== undefined)
            .join(',');

        const placeholder = args.map(arg => '?').join(',');

        db.execute('INSERT INTO ' + tableName + '(' + fields + ') VALUES (' + placeholder + ')', {
            args: args,
            argType: argType
        }).then(() => {
            let textMessage = req.get('Authorization') + ' inserted new row in ' + tableName +
                '. New row fields: ';
            Object.keys(row).forEach(field => textMessage += field + ' = ' + row[field] + ', ');
            logger.info(textMessage.slice(0, -2));
            res.end();
        }).catch((err) => {
            res.status(500).json(err).end();
        })
    } catch (err) {
        logger.error(err);
        if (!global.jdbcInited) {
            res.status(500).json("Can't connect to db").end();
        } else {
            res.status(500).json("Unexpected error occurred. Maybe config is incorrect").end();
        }
    }
});