const jsonParser = (require('body-parser')).json();

app.post('/getSettings', jsonParser, (req, res) => {
    try {
        const dictionaryData = {};
        const usedIds = {};
        const dictionaryPromiseList = [];
        const fileName = req.body.tableName;
        const tableName = fileToTableNamesMap[fileName];

        dictionaryNameList.forEach((dictionaryName) => {
            dictionaryData[dictionaryName] = [];
        });

        tableFieldList[fileName].forEach((field) => {
            const fieldSettings = tableFieldSettings[fileName][field];
            if (fieldSettings.isDictionaryValue) {
                const dictionaryName = fieldSettings.dictionaryName;
                const dictionaryIdField = global.dictionaryIdField[dictionaryName];
                const fields = dictionaryFieldList[dictionaryName].join(', ');
                const fieldType = {};

                dictionaryFieldList[dictionaryName].forEach((dictionaryField) => {
                    fieldType[dictionaryField] = dictionaryFieldSettings[dictionaryName][dictionaryField].type;
                });

                const promise = new Promise((resolve, reject) => {
                    db.execute('SELECT ' + fields + ' FROM ' + dictionaryName + ' WHERE ' + dictionaryIdField +
                        ' IN(SELECT ' + field + ' FROM ' + tableName + ')', {fieldType: fieldType})
                        .then((result) => {
                            usedIds[dictionaryName] = usedIds[dictionaryName] || {};
                            result.forEach((row) => {
                                if (!usedIds[dictionaryName][row[dictionaryIdField]]) {
                                    dictionaryData[dictionaryName].push(row);
                                    usedIds[dictionaryName][row[dictionaryIdField]] = true;
                                }
                            });
                            resolve();
                        }).catch(reject);
                });

                dictionaryPromiseList.push(promise);
            }
        });

        Promise.all(dictionaryPromiseList).then(() => {
            res.json({
                'tableConfig': tableConfig,
                'tableNameList': tableNameList,
                'dictionaryNameList': dictionaryNameList,
                'tableFieldList': tableFieldList,
                'dictionaryFieldList': dictionaryFieldList,
                'tableFieldSettings': tableFieldSettings,
                'dictionaryIdField': dictionaryIdField,
                'dictionaryValueField': dictionaryValueField,
                'dictionaryFieldSettings': dictionaryFieldSettings,
                'tableNamesMap': tableNamesMap,
                'fieldNamesMap': fieldNamesMap,
                'tableIdField': tableIdField,
                'dictionaryData': dictionaryData
            }).end();
        }).catch((err) => {
            res.status(500).json(err).end();
        });
    } catch (err) {
        logger.error(err);
        if (!global.jdbcInited) {
            res.status(500).json("Can't connect to db").end();
        } else {
            res.status(500).json("Unexpected error occurred. Maybe config is incorrect").end();
        }
    }
});

app.post('/getDictGroupsTableList', (req, res) => {
    try {
        res.json({
            'dictGroupsTableList': dictGroupsTableList,
            'tableNameList': tableNameList,
            'tableNamesMap': tableNamesMap
        }).end();
    } catch (err) {
        logger.error(err);
        if (!global.jdbcInited) {
            res.status(500).json("Can't connect to db").end();
        } else {
            res.status(500).json("Unexpected error occurred. Maybe config is incorrect").end();
        }
    }
});

app.post('/getDictionaryOptions', jsonParser, (req, res) => {
    try {
        const fileName = req.body.tableName;
        const row = req.body.row;
        const field = req.body.field;

        const fieldSettings = tableFieldSettings[fileName][field];

        const dictionaryName = fieldSettings.dictionaryName;
        const useFilter = fieldSettings.useFilter;

        const fieldType = {};

        dictionaryFieldList[dictionaryName].forEach((field) => {
            fieldType[field] = dictionaryFieldSettings[dictionaryName][field].type;
        });

        if (!useFilter) {
            const fields = dictionaryFieldList[dictionaryName].join(', ');
            db.execute('SELECT ' + fields + ' FROM ' + dictionaryName).then((result) => {
                result = result.sort((a, b) => {
                    return a[dictionaryValueField[dictionaryName]] < b[dictionaryValueField[dictionaryName]] ? -1 : 1;
                });
                res.json(result).end();
            }).catch((err) => {
                res.status(500).json(err).end();
            });
        } else {
            const fields = dictionaryFieldList[dictionaryName].join(', ');
            const args = [];
            const argType = {};

            let sql = 'SELECT ' + fields + ' FROM ' + dictionaryName + ' WHERE 1 = 1 ';

            if (fieldSettings.rules) {
                fieldSettings.rules.forEach((rule) => {
                    sql += ' AND ? ' + rule.operator + ' ' + rule.dictionaryField;
                    argType[args.length] = fieldSettings.type;
                    args.push(row[rule.field]);
                });
            }

            if (fieldSettings.staticRules) {
                fieldSettings.staticRules.forEach((staticRule) => {
                    sql += ' AND ? ' + staticRule.operator + ' ' + staticRule.dictionaryField;
                    argType[args.length] = dictionaryFieldSettings[dictionaryName][staticRule.dictionaryField].type;
                    args.push(staticRule.value);
                });
            }

            db.execute(sql, {args: args, argType: argType, fieldType: fieldType}).then((result) => {
                result = result.sort((a, b) => {
                    return a[dictionaryValueField[dictionaryName]] < b[dictionaryValueField[dictionaryName]] ? -1 : 1;
                });
                res.json(result).end();
            }).catch((err) => {
                res.status(500).json(err).end();
            });
        }
    } catch (err) {
        logger.error(err);
        if (!global.jdbcInited) {
            res.status(500).json("Can't connect to db").end();
        } else {
            res.status(500).json("Unexpected error occurred. Maybe config is incorrect").end();
        }
    }
});

app.post('/getDictGroups', (req, res) => {
    try {
        res.json(dictGroups).end();
    } catch (e) {
        logger.error(e);
        res.status(500).end();
    }
});

app.post('/checkTableAccessibility', jsonParser, (req, res) => {
    try {
        const fileName = req.body.tableName;

        db.checkTableAccessibility(fileName).then(() => {
            res.json({}).end();
        }).catch((err) => {
            logger.error(err);
            if (typeof err !== 'object') {
                res.json({error: err}).end();
            } else {
                if (!global.jdbcInited) {
                    res.status(500).json("Can't connect to db").end();
                } else {
                    res.status(500).json("Unexpected error occurred. Maybe config is incorrect").end();
                }
            }
        });
    } catch (err) {
        logger.error(err);
        if (!global.jdbcInited) {
            res.status(500).json("Can't connect to db").end();
        } else {
            res.status(500).json("Unexpected error occurred. Maybe config is incorrect").end();
        }
    }
});
