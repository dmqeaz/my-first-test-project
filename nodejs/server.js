require('dotenv').config();
const express = require('express');
const cors = require('cors');
const fs = require('fs');
const jinst = require('jdbc/lib/jinst');
const JDBC = require('jdbc');
const log4js = require('log4js');
const ncp = require('ncp');

const cluster = require('cluster');
const workersCount = 1;

log4js.configure({
    appenders: {
        out: {type: 'stdout'},
        app: {type: 'file', filename: 'application.log'}
    },
    categories: {
        default: {appenders: ['out', 'app'], level: 'debug'}
    }
});
global.logger = require('log4js').getLogger();

if (cluster.isMaster) {
    logger.info(`Master ${process.pid} is running`);

    const settingsFoundPromise = new Promise((resolve, reject) => {
        try {
            fs.readdirSync('./settings/table_config');
            logger.info("Using settings from mounted directory.");
            resolve();
        } catch (err) {
            logger.warn("Mounted settings directory empty. Using default settings.");

            ncp('./settings_default', './settings', (err) => {
                if (err) {
                    logger.error("Error while copying default settings.");
                    logger.error(err);
                    reject();
                }
                logger.info("Successfully copied default settings");
                resolve();
            });
        }
    });

    settingsFoundPromise.then(() => {
        const deadWorkers = [];

        for (let i = 0; i < workersCount; i++) {
            cluster.fork();
        }

        cluster.on('exit', (worker, code, signal) => {
            logger.warn(`Worker ${worker.process.pid} died. code = ${code} signal = ${signal}`);

            deadWorkers.push(+new Date());
            if (deadWorkers.filter(time => +new Date() - time < 60 * 1000).length > 10) {
                logger.error('Something went wrong. Too many dead workers');
                throw 'Too many dead workers error';
            }

            logger.warn('Trying to create new worker');
            try {
                cluster.fork();
            } catch (err) {
                logger.warn('Error while creating new worker \n' + err);
            }
        });
    });
} else {
    initWorker();
    logger.info(`Worker ${process.pid} started`);
}

function initWorker() {
    const settings = process.env;
    global.AD_DOMAIN = settings.AD_DOMAIN;
    global.LDAP_URL = settings.LDAP_URL;
    global.APP_SERVER_PORT = settings.APP_SERVER_PORT;
    global.logger.level = settings.APP_LOG_LEVEL || 'error';

    global.app = express();
    app.use(cors());
    app.use(express.static(__dirname + '/public'));

    require('./modules/auth');
    require('./modules/config');
    require('./modules/edit_data');
    require('./modules/get_settings');

    app.get('*', (req, res) => {
        res.sendFile(__dirname + '/public/index.html');
    });

    app.all('/health', (req, res) => {
        res.send({
            status: 'healthy'
        }).end();
    });

    app.listen(APP_SERVER_PORT || 3000, () => {
        logger.info('Listening port ' + (APP_SERVER_PORT || 3000));
    });

    initJDBC().then(() => {
        initDataSettings();
        logger.info('Data settings loaded');
        db.checkDictionarySettings();
    });

    function initJDBC() {
        return new Promise((resolve, reject) => {
            global.db = require('./modules/db');
            if (!jinst.isJvmCreated()) {
                jinst.addOption("-Xrs");
                jinst.setupClasspath(['./drivers/' + settings.DB_DRIVER]);
            }

            const jdbc = new JDBC({
                url: settings.DB_CONNECT_STRING,
                minpoolsize: 3,
                maxpoolsize: 10,
                user: settings.DB_USER,
                password: settings.DB_PASSWORD,
                maxidle: 60 * 60 * 1000
            });

            const tryInit = () => {
                jdbc.initialize(function (err) {
                    if (err) {
                        logger.error(err);
                        reject();
                    } else {
                        logger.info('Successfully connected to db using jdbc');
                        global.db.jdbc = jdbc;
                        global.jdbcInited = true;
                        resolve();
                    }
                });
            };

            tryInit();
        });
    }

    function initDataSettings() {
        global.tableNameList = [];
        global.dictionaryNameList = [];
        global.dictGroups = [];
        global.dictGroupsTableList = [];

        const tableFieldList = {};
        const dictionaryFieldList = {};
        const tableFieldSettings = {};
        const dictionaryIdField = {};
        const dictionaryValueField = {};
        const dictionaryFieldSettings = {};
        const tableNamesMap = {};
        const fieldNamesMap = {};
        const tableIdField = {};
        const tableConfig = {};
        const fileToTableNamesMap = {};
        const tableFilterRules = {};

        try {
            fs.readdirSync('./settings/table_config').forEach((fileName) => {
                const tableData = JSON.parse(fs.readFileSync('./settings/table_config/' + fileName, 'utf-8'));
                fileToTableNamesMap[fileName] = tableData.dbSchema + '.' + tableData.tableName;
                tableConfig[fileName] = {
                    isNewRowAllowed: tableData.isNewRowAllowed,
                    dictGroupId: tableData.dictGroupId || 0
                };
                tableNameList.push(fileName);
                tableFieldList[fileName] = [];
                tableFieldSettings[fileName] = {};
                fieldNamesMap[fileName] = {};
                tableNamesMap[fileName] = tableData.displayName;
                tableIdField[fileName] = tableData.idField;
                tableFilterRules[fileName] = tableData.filterRules || [];

                tableData.fields.forEach((field) => {
                    if (field.isDictionaryValue) {
                        field.dictionaryName = field.dictionaryDBSchema + '.' + field.dictionaryName;
                    }
                    tableFieldList[fileName].push(field.name);
                    tableFieldSettings[fileName][field.name] = field;
                    fieldNamesMap[fileName][field.name] = field.displayName;
                });
            });
        } catch (err) {
            logger.error(err);
        }

        try {
            fs.readdirSync('./settings/dictionary_config').forEach((fileName) => {
                const dictionaryData = JSON.parse(fs.readFileSync('./settings/dictionary_config/' + fileName, 'utf-8'));
                const dictionaryName = dictionaryData.dbSchema + '.' + dictionaryData.tableName;
                dictionaryNameList.push(dictionaryName);
                dictionaryFieldSettings[dictionaryName] = {};
                dictionaryFieldList[dictionaryName] = dictionaryData.fields.map(field => field.name);
                dictionaryIdField[dictionaryName] = dictionaryData.idField;
                dictionaryValueField[dictionaryName] = dictionaryData.valueField;

                dictionaryData.fields.forEach((field) => {
                    dictionaryFieldSettings[dictionaryName][field.name] = field;
                });
            });
        } catch (err) {
            logger.warn(err);
        }

        global.tableFieldList = tableFieldList;
        global.dictionaryFieldList = dictionaryFieldList;
        global.tableFieldSettings = tableFieldSettings;
        global.dictionaryIdField = dictionaryIdField;
        global.dictionaryValueField = dictionaryValueField;
        global.dictionaryFieldSettings = dictionaryFieldSettings;
        global.tableNamesMap = tableNamesMap;
        global.fieldNamesMap = fieldNamesMap;
        global.tableIdField = tableIdField;
        global.tableConfig = tableConfig;
        global.fileToTableNamesMap = fileToTableNamesMap;
        global.tableFilterRules = tableFilterRules;

        try {
            const dictGroupsList = JSON.parse(fs.readFileSync('./settings/dict_groups.json', 'utf-8')).dictGroups;
            dictGroupsList.sort((a, b) => {
                return a['sortId'] < b['sortId'] ? -1 : 1;
            });
            dictGroups = JSON.parse(JSON.stringify(dictGroupsList));
            dictGroupsTableList = JSON.parse(JSON.stringify(dictGroupsList));
            let emptyDictGroup = {};
            emptyDictGroup.id = 0;
            emptyDictGroup.name = "Без категории";
            dictGroupsTableList.push(emptyDictGroup);
            for (let i = 0, len = dictGroupsTableList.length; i < len; i++) {
                dictGroupsTableList[i].tableNameList = [];
                for (let fileName in tableConfig) {
                    if (tableConfig[fileName].dictGroupId === dictGroupsTableList[i].id) {
                        dictGroupsTableList[i].tableNameList.push(fileName);
                    }
                }
                dictGroupsTableList[i].tableNameList.sort();
            }
        } catch (err) {
            logger.warn(err);
        }
    }

    global.reloadConfig = (fileName, prevFileName, prevDictGroupId) => {
        if (typeof prevFileName !== 'undefined' && fileName !== prevFileName) {
            tableNameList.splice(tableNameList.indexOf(prevFileName), 1);
            delete fileToTableNamesMap[prevFileName];
            delete tableConfig[prevFileName];
            delete tableFieldList[prevFileName];
            delete tableFieldSettings[prevFileName];
            delete fieldNamesMap[prevFileName];
            delete tableNamesMap[prevFileName];
            delete tableIdField[prevFileName];
            delete tableFilterRules[prevFileName];
        }

        if (tableNameList.find((t) => t === fileName) === undefined) {
            tableNameList.push(fileName);
        }

        const tableData = JSON.parse(fs.readFileSync('./settings/table_config/' + fileName, 'utf-8'));

        fileToTableNamesMap[fileName] = tableData.dbSchema + '.' + tableData.tableName;
        tableConfig[fileName] = {
            isNewRowAllowed: tableData.isNewRowAllowed,
            dictGroupId: tableData.dictGroupId || 0
        };

        let newDictGroupId = tableConfig[fileName].dictGroupId;
        if (fileName !== prevFileName || newDictGroupId !== prevDictGroupId) {
            for (let i = 0, len = dictGroupsTableList.length, deleted = false, pushed = false;
                 i < len && !(deleted && pushed); i++) {
                if (!deleted && typeof prevFileName !== 'undefined' &&
                    dictGroupsTableList[i].id === prevDictGroupId) {
                    let index = dictGroupsTableList[i].tableNameList.indexOf(prevFileName);
                    dictGroupsTableList[i].tableNameList.splice(index, 1);
                    deleted = true;
                }
                if (!pushed && dictGroupsTableList[i].id === newDictGroupId) {
                    dictGroupsTableList[i].tableNameList.push(fileName);
                    dictGroupsTableList[i].tableNameList.sort();
                    pushed = true;
                }
            }
        }

        tableFieldList[fileName] = [];
        tableFieldSettings[fileName] = {};
        fieldNamesMap[fileName] = {};
        tableNamesMap[fileName] = tableData.displayName;
        tableIdField[fileName] = tableData.idField;
        tableFilterRules[fileName] = tableData.filterRules || [];

        tableData.fields.forEach((field) => {
            if (field.isDictionaryValue) {
                field.dictionaryName = field.dictionaryDBSchema + '.' + field.dictionaryName;
            }
            tableFieldList[fileName].push(field.name);
            tableFieldSettings[fileName][field.name] = field;
            fieldNamesMap[fileName][field.name] = field.displayName;
        });
    };
}